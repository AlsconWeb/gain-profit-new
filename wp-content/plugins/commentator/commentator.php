<?php
/*
Plugin Name: Commentator
Plugin URI: http://codecanyon.net/item/commentator-wordpress-plugin/6425752
Description: Commentator Wordpress Plugin is a fully featured ajaxified comments system for your wordpress website that let you easily supercharge your discussions.
Author: Yukulelix
Author URI: http://codecanyon.net/user/yukulelix
Version: 2.7.0
Text Domain: commentator
Domain Path: /lang
*/

// Make sure that no info is exposed if file is called directly -- Idea taken from Akismet plugin
if ( !function_exists( 'add_action' ) ) {
	_e( 'This page cannot be called directly.', 'commentator' );
	exit;
}

// Define some useful constants that can be used by functions
if ( ! defined( 'WP_CONTENT_URL' ) ) {	
	if ( ! defined( 'WP_SITEURL' ) ) define( 'WP_SITEURL', get_option("siteurl") );
	define( 'WP_CONTENT_URL', WP_SITEURL . '/wp-content' );
}
if ( ! defined( 'WP_SITEURL' ) ) define( 'WP_SITEURL', get_option("siteurl") );
if ( ! defined( 'WP_CONTENT_DIR' ) ) define( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );
if ( ! defined( 'WP_PLUGIN_URL' ) ) define( 'WP_PLUGIN_URL', WP_CONTENT_URL. '/plugins' );
if ( ! defined( 'WP_PLUGIN_DIR' ) ) define( 'WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins' );

if ( ! defined( 'SPEC_COMMENT_TMP' ) ) define('SPEC_COMMENT_TMP', plugins_url( '/php/commentator-template.php', __FILE__ ));

if ( basename(dirname(__FILE__)) == 'plugins' )
	define("COMMENTATOR_DIR",'');
else define("COMMENTATOR_DIR" , basename(dirname(__FILE__)) . '/');
define("COMMENTATOR_PATH", WP_PLUGIN_URL . "/" . COMMENTATOR_DIR);
/* Add new menu */
add_action('admin_menu', 'commentator_add_pages');
/* Register Settings */
add_action( 'admin_init', 'register_commentator_settings' );
 
require_once( WP_PLUGIN_DIR . '/' . COMMENTATOR_DIR . 'hybridauth/Hybrid/Auth.php' );

add_role(
    'commentator_commenter',
    __( 'Commentator Commenter' , 'commentator' ),
    array(
        'read'         => false,
        'edit_posts'   => false,
        'delete_posts' => false,
    )
);

add_image_size("commentator", 500, 500);

function register_session(){
    if( !session_id())
        session_start();
}
add_action('init','register_session');

function commentator_translation_init() {
	$plugin_dir = basename(dirname(__FILE__));
	load_plugin_textdomain( 'commentator', false, $plugin_dir."/lang" );
}
add_action('plugins_loaded', 'commentator_translation_init');
/*

******** BEGIN PLUGIN FUNCTIONS ********

*/

add_action('wp_head','commentator_ajaxurl');
function commentator_ajaxurl() {
?>
<script type="text/javascript">
	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<?php
}

function roles_and_capabilities(){
	$roles = get_option('commentator_disabled-user-roles', array());
	$roles = is_array($roles) ? $roles : array();
	global $wp_roles;
    $allRoles = $wp_roles->get_names();
	foreach ($allRoles as $role_name => $role_info){
		if(!in_array( $role_name, $roles ))
			get_role($role_name)->add_cap('commentator-comment');
		else
			get_role($role_name)->remove_cap('commentator-comment');
	}
}
add_action('wp_loaded', 'roles_and_capabilities');

add_action( 'wp_ajax_commentator_register', 'commentator_register' );
add_action('wp_ajax_nopriv_commentator_register', 'commentator_register' );
function commentator_register() {
	$errors = array();
	$arr = array();
	$username = isset($_POST['username']) ? $_POST['username'] : "";
	if(empty($username)) {  
        $errors[] = __( 'User name should not be empty.', 'commentator' );  
    }
    $email = isset($_POST['email']) ? $_POST['email'] : "";
    if(!$email || strlen($email) == 0 || ctype_space($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {  
        $errors[] = __( 'The email is empty or invalid.', 'commentator' );  
    }
    $password = isset($_POST['password']) ? $_POST['password'] : "";
    if(get_option('commentator_register_password_chose')){
    	if(!$password || strlen($password) == 0 || ctype_space($password)) {  
        	$errors[] = __( 'You need to chose a password.', 'commentator' );  
    	}
    }
    else{
    	$password = wp_generate_password( 12, false );
    }

    if(count($errors) == 0){
	    $status = wp_create_user( $username, $password, $email );
	    if ( is_wp_error($status) ) {
	        $errors[] = $status->get_error_message();
	    }
	}

    if(count($errors) == 0){
    	$user = get_userdata( $status );
    	$user->set_role('commentator_commenter');
        $from = get_option('admin_email');  
        $headers = __( 'From', 'commentator' ).' '.$from . "</br>";  
        $subject = __( 'Registration successful', 'commentator' );
        $msg = __( 'Registration successful', 'commentator' ).".<br/>".__( 'Your login details', 'commentator' )."\n".__( 'Username', 'commentator' ).": ".$username."\n".__( 'Password', 'commentator' ).": ".$password;
        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        wp_mail( $email, $subject, $msg, $headers );
        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
        $arr = array(
			'message' => __( 'Registration successful, check your email for your password', 'commentator' )
		);
    }
    else{
    	$arr = array(
			'errors' => $errors,
		);
    }
	wp_send_json($arr);
	die();
}



add_action( 'wp_ajax_commentator_login', 'commentator_login' );
add_action('wp_ajax_nopriv_commentator_login', 'commentator_login' );
function commentator_login() {
	$errors = array();
	$arr = array();
	$username = isset($_POST['username']) ? $_POST['username'] : "";
	$password = isset($_POST['password']) ? $_POST['password'] : "";

	$creds = array();
	$creds['user_login'] = $username;
	$creds['user_password'] = $password;
	$creds['remember'] = isset($_POST['remember']);
	$user = wp_signon( $creds, false );

	if ( is_wp_error($user) )
		$errors[] = $user->get_error_message();

    if(count($errors) == 0){
        $arr = array(
			'avatar' => get_avatar( $user->ID ),
			'username' => $user->user_login,
			'ID' => $user->ID
		);
    }  
    else{
    	$arr = array(
			'errors' => $errors,
		);
    }
	wp_send_json($arr);
	die();
}

add_action( 'wp_ajax_commentator_logout', 'commentator_logout' );
add_action('wp_ajax_nopriv_commentator_logout', 'commentator_logout' );
function commentator_logout() {
	if(is_user_logged_in()){
		wp_logout();
	}
	elseif(isset($_SESSION['commentator_user_profile']) && $_SESSION["commentator_user_profile"] != null){
			Hybrid_Auth::logoutAllProviders();
			$_SESSION["commentator_user_profile"] = null;
			$_SESSION["commentator_provider"] = null;
	}
	die();
}

add_action( 'wp_ajax_commentator_vote-thread', 'commentator_voteThread' );
add_action('wp_ajax_nopriv_commentator_vote-thread', 'commentator_voteThread' );
function commentator_voteThread() {
	$upVotes = get_post_meta( $_POST['comment_post_ID'], "upVote-discussion" );
	$count = count($upVotes);
	$hasVoted = false;
	$errors = array();
	$arr = array();
	$content_post = get_post($_POST['comment_post_ID']);

	global $current_user;
	get_currentuserinfo();

	$voteAuthorID = $current_user->ID;

	$user_profile = isset($_SESSION['commentator_user_profile']) ? $_SESSION["commentator_user_profile"] : null;
	if(!is_user_logged_in() && $user_profile != null){
		$voteAuthorID = $_SESSION["commentator_provider"]."-".$user_profile['identifier'];
	}

	if(!$content_post){
		$errors[] = __( 'the post doesn\'t exist', 'commentator' );
	}

	if(!is_user_logged_in() && $user_profile == null){
		$errors[] = __( 'you are not logged in !', 'commentator' );
	}

	if(count($errors) == 0){
	    if(!in_array ( $voteAuthorID, $upVotes )){
			add_post_meta($_POST['comment_post_ID'], "upVote-discussion", $voteAuthorID);
	    	$hasVoted = true;
		}
		else{
			delete_post_meta( $_POST['comment_post_ID'], "upVote-discussion", $voteAuthorID );
		}

		$upVotes = get_post_meta( $_POST['comment_post_ID'], "upVote-discussion" );
		$count = count($upVotes);

		$arr = array(
			'count' => $count,
			'hasVoted' => $hasVoted
		);
	}

	else{
		$arr = array(
			'errors' => $errors,
		);
	}

	wp_send_json($arr);
	die();
}

add_action( 'wp_ajax_commentator_flag', 'commentator_flag' );
add_action('wp_ajax_nopriv_commentator_flag', 'commentator_flag' );
function commentator_flag() {
	$comment_ID = $_POST['comment_ID'];
	$comment = get_comment( $comment_ID );
	$post  = get_post( $comment->comment_post_ID );
	$errors = array();
	$arr = array();

	global $current_user;
	get_currentuserinfo();

	$voteAuthorID = $current_user->ID;
	$user_profile = isset($_SESSION['commentator_user_profile']) ? $_SESSION["commentator_user_profile"] : null;
	if(!is_user_logged_in() && $user_profile != null){
		$voteAuthorID = $_SESSION["commentator_provider"]."-".$user_profile['identifier'];
	}

	if(!is_user_logged_in() && $user_profile == null){
		$errors[] = __( 'you are not logged in !', 'commentator' );
	}

	if(!get_option('commentator_flag-comment')){
		$errors[] = __( 'Flagging is not allowed !', 'commentator' );
	}

	if(count($errors) == 0){
	    update_comment_meta($comment_ID, "flag-comment", $voteAuthorID);
	    $link = get_comment_link( $comment->comment_ID );
	    $count = count(get_comment_meta($comment_ID, "flag-comment"));
	    update_comment_meta($comment_ID, "flag-comment-count", $count);

	    if(get_option('commentator_flag-limit') && $count >= ((int) get_option('commentator_flag-limit'))){
	    	$commentarr = array();
			$commentarr['comment_ID'] = $comment_ID; // This is the only required array key
			$commentarr['comment_approved'] = 0;
			wp_update_comment( $commentarr );
		}

	    $to = get_option('admin_email');  
        $headers = __( 'From', 'commentator' ).' '.$to . "</br>";  
        $subject = __( 'New Comment Flagging', 'commentator' );
        $msg = 
        	__( 'A new comment has been flagged on ', 'commentator' ).'<a href="'.$link.'">'.$post->post_title.'</a>'.
			".<br/>".
        	__( 'Number of flags : ', 'commentator' ).$count.
        	".<br/>".
        	"<pre>".$comment->comment_content."</pre>".
        	"<br/>".
        	__( 'Go to your wordpress admin to see it', 'commentator' );
        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        wp_mail( $to, $subject, $msg, $headers );
        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
		$arr = array(
			'message' => __('The comment has been flagged', 'commentator')
		);
	}

	else{
		$arr = array(
			'errors' => $errors,
		);
	}

	wp_send_json($arr);
	die();
}

add_action( 'wp_ajax_commentator_delete', 'commentator_delete' );
add_action('wp_ajax_nopriv_commentator_delete', 'commentator_delete' );
function commentator_delete() {
	$comment_ID = $_POST['comment_ID'];
	$comment = get_comment( $comment_ID );
	$errors = array();
	$arr = array();

	if(get_option('commentator_social-signin')){
		$user_profile = isset($_SESSION['commentator_user_profile']) ? $_SESSION["commentator_user_profile"] : null;
		if($user_profile != null){
			$user_email = $user_profile['email'];
		}
	}

	// If the user is logged in
	$user = wp_get_current_user();
	if ( $user->exists() ) {
		$user_email = wp_slash( $user->user_email );
	} else {
		if ( $user_profile==null ){
			$errors[] = __('you are not logged in !', 'commentator');
		}
	}

	if ( $comment->comment_author_email != $user_email ){
		$errors[] = __('Sorry, this is not your comment', 'commentator');
	}

	if(count($errors) == 0){
		wp_delete_comment( $comment_ID );
		$arr = array(
			'message' => __('This comment has been deleted', 'commentator')
		);
	}
	else{
		$arr = array(
			'errors' => $errors,
		);
	}

	wp_send_json($arr);
	die();
}

add_action( 'wp_ajax_commentator_pick', 'commentator_pick' );
add_action('wp_ajax_nopriv_commentator_pick', 'commentator_pick' );
function commentator_pick() {
	$comment_ID = $_POST['comment_ID'];
	$comment = get_comment( $comment_ID );
	$post = get_post( $comment->comment_post_ID );
	$authorID = $post->post_author;
	$author = get_user_by( 'id', $authorID );
	$errors = array();
	$arr = array();

	if(get_option('commentator_social-signin')){
		$user_profile = isset($_SESSION['commentator_user_profile']) ? $_SESSION["commentator_user_profile"] : null;
		if($user_profile != null){
			$user_email = $user_profile['email'];
		}
	}

	// If the user is logged in
	$user = wp_get_current_user();
	if ( $user->exists() ) {
		$user_email = wp_slash( $user->user_email );
	} else {
		if ( $user_profile==null ){
			$errors[] = __('you are not logged in !', 'commentator');
		}
	}

	if ( $author->user_email != $user_email ){
		$errors[] = __('Sorry, this is not your post', 'commentator');
	}

	if(count($errors) == 0){
		$pickedComments = get_post_meta ( $post->ID, "pickedComments");
		$isPicked = in_array ( $comment_ID, $pickedComments );

		if($isPicked){
			delete_post_meta( $post->ID, "pickedComments", $comment_ID);
			$arr = array(
				'message' => __('This comment has been unpicked', 'commentator')
			);
		}
		else{
			add_post_meta( $post->ID, "pickedComments", $comment_ID);
			$arr = array(
				'message' => __('This comment has been picked', 'commentator')
			);
		}
	}
	else{
		$arr = array(
			'errors' => $errors,
		);
	}

	wp_send_json($arr);
	die();
}


add_action( 'wp_ajax_commentator_subscribe', 'commentator_subscribe' );
add_action('wp_ajax_nopriv_commentator_subscribe', 'commentator_subscribe' );
function commentator_subscribe(){
	$comment_ID = isset($_POST['comment_ID']) ? $_POST['comment_ID'] : 0;
	$post_ID = isset($_POST['post_ID']) ? $_POST['post_ID'] : 0;
	$user = wp_get_current_user();
	if ( $user->exists() ) {
		$subscriberEmail = $user->user_email;
		$subscriptions = isset($_POST['post_ID']) ? get_post_meta($post_ID, "commentator_subscriptions") : get_comment_meta($comment_ID, "commentator_subscriptions");
		$array = array();
		if(get_option('commentator_subscribe')){
			if(!in_array ( $subscriberEmail, $subscriptions )){
				if(isset($_POST['post_ID'])){
		    		add_post_meta( $post_ID, "commentator_subscriptions", $subscriberEmail);
		    		$array["message"] = __( 'You are now subscribed to that post', 'commentator' );
				}
				else{
					add_comment_meta( $comment_ID, "commentator_subscriptions", $subscriberEmail);
		    		$array["message"] = __( 'You are now subscribed to that comment', 'commentator' );
				}
			}
			else{
				if(isset($_POST['post_ID'])){
					delete_post_meta( $post_ID, "commentator_subscriptions", $subscriberEmail );
					$array["message"] = __( 'You are now unsubscribed from that post', 'commentator' );
				}
				else{
					delete_comment_meta( $comment_ID, "commentator_subscriptions", $subscriberEmail );
					$array["message"] = __( 'You are now unsubscribed from that comment', 'commentator' );
				}
			}
		}
		else{
			$array["errors"] = array();
			$array["errors"][] = __( 'Subscription is not allowed', 'commentator' );
		}
	}
	else {
		$array["errors"] = array();
		$array["errors"][] = __( 'You must be logged in to subscribe', 'commentator' );
	}
	wp_send_json($array);
    die();
}

add_action('transition_comment_status', 'commentator_approve_comment_callback', 10, 3);
function commentator_approve_comment_callback($new_status, $old_status, $comment) {
    if($old_status != $new_status) {
        if($new_status == 'approved') {
            delete_comment_meta($comment->comment_ID, "flag-comment");
        }
    }
}

function set_html_content_type()
{
    return 'text/html';
}

add_action( 'wp_ajax_commentator_social_signin', 'commentator_social_signin' );
add_action('wp_ajax_nopriv_commentator_social_signin', 'commentator_social_signin' );
function commentator_social_signin(){
	if( isset($_REQUEST["provider"]) ){ 
		// the selected provider
		$provider_name = htmlentities($_REQUEST["provider"]);
		try{
			// initialize Hybrid_Auth with a given file
			$hybridauth = new Hybrid_Auth( 
				array(
					"base_url" => WP_PLUGIN_URL . '/' . COMMENTATOR_DIR . 'hybridauth/', 

					"providers" => array ( 
						// openid providers

						"Google" => array ( 
							"enabled" => get_option('commentator_id_key-google') && get_option('commentator_secret_key-google'),
							"keys"    => array ( "id" => get_option('commentator_id_key-google'), "secret" => get_option('commentator_secret_key-google') ), 
						),

						"Facebook" => array ( 
							"enabled" => get_option('commentator_id_key-facebook') && get_option('commentator_secret_key-facebook'),
							"scope"   => "email",
							"keys"    => array ( "id" => get_option('commentator_id_key-facebook'), "secret" => get_option('commentator_secret_key-facebook') ), 
						),

						"Twitter" => array ( 
							"enabled" => get_option('commentator_id_key-twitter') && get_option('commentator_secret_key-twitter'),
							"keys"    => array ( "key" => get_option('commentator_id_key-twitter'), "secret" => get_option('commentator_secret_key-twitter') ), 
						),

						"LinkedIn" => array ( 
							"enabled" => get_option('commentator_id_key-linkedin') && get_option('commentator_secret_key-linkedin'),
							"keys"    => array ( "key" => get_option('commentator_id_key-linkedin'), "secret" => get_option('commentator_secret_key-linkedin') ), 
						),
					),

					// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
					"debug_mode" => false,

					"debug_file" => "",
				)	
			);
 
			// try to authenticate with the selected provider
			$adapter = $hybridauth->authenticate( $provider_name );
 
			// then grab the user profile 
			$user_profile = $adapter->getUserProfile();

			$_SESSION["commentator_user_profile"] = (array) $user_profile;
			$_SESSION["commentator_provider"] = $provider_name;
		}
		catch( Exception $e ){
			$caught = true;
		}
?>
	<!DOCTYPE html>
	<html lang="en">
		<head>
		    <meta charset="utf-8">
		    <title><?php _e("Commentator Social Signin", "commentator"); ?></title>
		    <style>
			    body, html{
			    	margin: 0;
			    	padding: 0;
					width: 100%;
					height: 100%;
  					font-family: 'Helvetica Neue', arial, sans-serif;
			    }
			    #commentator-social-signin{
			    	position: relative;
					width: 100%;
					height: 100%;
			    }
				.commentator-social-message{
					color: #fff;
					position: absolute;
					top: 0;
					left: 0;
					width: 100%;
					height: 100%;
		  			display: table; 
				}
				#commentator-social-signin .commentator-social-message > div{
					display: table-cell; 
		  			vertical-align: middle; 
		  			text-align: center; 
		  			text-align: center;
				}
				h4{
					opacity: .8;
				}
				.commentator-facebook .commentator-social-message{
					background: #4862A3;
				}
				.commentator-twitter .commentator-social-message{
					background: #00ACEE;
				}
				.commentator-google .commentator-social-message{
				  background: #C13222;
				}
				.commentator-linkedin .commentator-social-message{
				  background: #0073B2;
				}
			</style>
		</head>
		<body>
		    <div id="commentator-social-signin" class="commentator-<?php echo $provider_name; ?>">

		    	<div class="commentator-social-message">
			    	<div id="commentator-social-loading">
			    		<div>
			    			<?php _e("Connecting...", "commentator"); ?>
			    		</div>
					</div>
				</div>

		<?php
		if($caught){
		?>
			<div class="commentator-social-message">
				<div>
					<h1><?php _e("Error: please try again!", "commentator"); ?></h1>
					<h4><?php _e("Original error message: ", "commentator"); ?> <?php echo $e->getMessage(); ?></h4>
				</div>
			</div>
			<?php
		}else{
	?>
			<div class="commentator-social-message">
				<div>
					<h1><?php _e('Congratulations !', "commentator"); ?></h1>
					<h4><?php _e('You can now join the conversation !', "commentator"); ?></h4>
				</div>
			</div>

			<script type="text/javascript">
				window.opener.location.reload();
				setTimeout(function(){
					window.close();
				}, 3000);
			</script>
		<?php
		}
		?>
		</body>
	</html>
	<?php
	}
	die();
}

add_action( 'wp_ajax_commentator_upload-image', 'commentator_upload_image' );
add_action('wp_ajax_nopriv_commentator_upload-image', 'commentator_upload_image' );
function commentator_upload_image() {
	$errors = array();
	$arr = array();
	// A list of permitted file extensions
	$allowed = array('png', 'jpg', 'gif');
	$upload_dir = wp_upload_dir();

	if(get_option('commentator_allow-images')){

		if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

			$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

			if(!in_array(strtolower($extension), $allowed)){
				$errors[] = __( 'This type of image is not allowed', 'commentator' );
			}

			$attachment_id = media_handle_upload('upl', array( 'test_form' => false ));
	
			if ( is_wp_error( $attachment_id ) ) {
				$errors[] = __( 'There was an error uploading the image', 'commentator' );
			} else {
				$url = wp_get_attachment_image_src( $attachment_id, "commentator" )[0];
			}
		}
		else{
			$errors[] = __( 'There was an error uploading the image', 'commentator' );
		}

	}
	else{
		$errors[] = __( 'Images upload is not allowed', 'commentator' );
	}



	if(count($errors) == 0){
		$arr = array(
			'url' => $url
		);
	}
	else{
		$arr = array(
			'errors' => $errors,
		);
	}

	wp_send_json($arr);
	die();
}

add_action( 'wp_ajax_commentator_add-comment', 'commentator_add_comment' );
add_action('wp_ajax_nopriv_commentator_add-comment', 'commentator_add_comment' );
function commentator_add_comment() {

	$comment_post_ID = isset($_POST['comment_post_ID']) ? (int) $_POST['comment_post_ID'] : 0;

	$post = get_post($comment_post_ID);

	if ( empty( $post->comment_status ) ) {
		/**
		 * Fires when a comment is attempted on a post that does not exist.
		 *
		 * @since unknown
		 * @param int $comment_post_ID Post ID.
		 */
		do_action( 'comment_id_not_found', $comment_post_ID );
		exit;
	}

	// get_post_status() will get the parent status for attachments.
	$status = get_post_status($post);

	$status_obj = get_post_status_object($status);

	if ( ! comments_open( $comment_post_ID ) ) {
		/**
		 * Fires when a comment is attempted on a post that has comments closed.
		 *
		 * @since unknown
		 * @param int $comment_post_ID Post ID.
		 */
		do_action( 'comment_closed', $comment_post_ID );
		wp_die( __('Sorry, comments are closed for this item.') );
	} elseif ( 'trash' == $status ) {
		/**
		 * Fires when a comment is attempted on a trashed post.
		 *
		 * @since 2.9.0
		 * @param int $comment_post_ID Post ID.
		 */
		do_action( 'comment_on_trash', $comment_post_ID );
		exit;
	} elseif ( ! $status_obj->public && ! $status_obj->private ) {
		/**
		 * Fires when a comment is attempted on a post in draft mode.
		 *
		 * @since unknown
		 * @param int $comment_post_ID Post ID.
		 */
		do_action( 'comment_on_draft', $comment_post_ID );
		exit;
	} elseif ( post_password_required( $comment_post_ID ) ) {
		/**
		 * Fires when a comment is attempted on a password-protected post.
		 *
		 * @since unknown
		 * @param int $comment_post_ID Post ID.
		 */
		do_action( 'comment_on_password_protected', $comment_post_ID );
		exit;
	} else {
		/**
		 * Fires before a comment is posted.
		 *
		 * @since unknown
		 * @param int $comment_post_ID Post ID.
		 */
		do_action( 'pre_comment_on_post', $comment_post_ID );
	}

	$comment_author       = ( isset($_POST['author-name']) )  ? trim(strip_tags($_POST['author-name'])) : null;
	$comment_author_email = ( isset($_POST['author-email']) )   ? trim($_POST['author-email']) : null;
	$comment_author_url   = ( isset($_POST['author-url']) )     ? trim($_POST['author-url']) : null;
	$comment_content      = ( isset($_POST['comment']) ) ? trim($_POST['comment']) : null;
	$user_ID = 0;
	$comment_image = ( isset($_POST['attached-image']) ) ? trim($_POST['attached-image']) : null;

	$user_profile = null;
	if(get_option('commentator_social-signin')){
		$user_profile = isset($_SESSION['commentator_user_profile']) ? $_SESSION["commentator_user_profile"] : null;
		if($user_profile != null){
			$comment_author = $user_profile['displayName'];
			$comment_author_url = $user_profile['webSiteURL'] ? $user_profile['webSiteURL'] : $user_profile['profileURL'];
			$comment_author_email = $user_profile['email'];
		}
	}

	// If the user is logged in
	$user = wp_get_current_user();
	if ( $user->exists() ) {
		if ( empty( $user->display_name ) )
			$user->display_name=$user->user_login;
		$comment_author       = wp_slash( $user->display_name );
		$comment_author_email = wp_slash( $user->user_email );
		$comment_author_url   = wp_slash( $user->user_url );
		$user_ID   = wp_slash( $user->ID );
		if(!current_user_can( 'commentator-comment' )){
			wp_die( __('Sorry, your user role doesn\'t allow you to comment.', 'commentator') );
		}
		if ( current_user_can( 'unfiltered_html' ) ) {
			if ( ! isset( $_POST['_wp_unfiltered_html_comment'] )
				|| ! wp_verify_nonce( $_POST['_wp_unfiltered_html_comment'], 'unfiltered-html-comment_' . $comment_post_ID )
			) {
				kses_remove_filters(); // start with a clean slate
				kses_init_filters(); // set up the filters
			}
		}
	} else {
		if ( $user_profile==null && (get_option('comment_registration') || 'private' == $status) )
			wp_die( __('Sorry, you must be logged in to post a comment.') );
	}

	$comment_type = '';

	if ( get_option('require_name_email') && !$user->exists() && $user_profile == null ) {
		if ( 6 > strlen($comment_author_email) || '' == $comment_author )
			wp_die( __('<strong>ERROR</strong>: please fill the required fields (name, email).') );
		elseif ( !is_email($comment_author_email))
			wp_die( __('<strong>ERROR</strong>: please enter a valid email address.') );
	}

	if ( '' == $comment_content && $comment_image==null )
		wp_die( __('<strong>ERROR</strong>: please type a comment.') );


	//$comment_content = preg_replace('/(?:(?:\r\n|\r|\n)\s*){2}/s', "\n\n", $comment_content);
	$comment_content = nl2br($comment_content);

	$comment_parent = isset($_POST['comment_parent']) ? absint($_POST['comment_parent']) : 0;

	if(get_option("commentator_subscribe")){
		if(isset($_POST['comment_parent']) && $_POST['comment_parent'] > 0){
			commentator_advise_subscribers($comment_parent);
		}
		else{
			commentator_advise_post_subscribers($comment_post_ID);
		}
	}

	$commentdata = compact('comment_post_ID', 'comment_author', 'comment_author_email', 'comment_author_url', 'comment_content', 'comment_type', 'comment_parent', 'user_ID');

	$comment_id = wp_new_comment( $commentdata );
	$comment = get_comment($comment_id);

	if(get_option('commentator_allow-images') && $comment_image!=null ){
		update_comment_meta( $comment_id, "commentator-attached-image", $comment_image);
	}

	if($user_profile != null){
		update_comment_meta( $comment_id, "commentator-social-avatar", $user_profile['photoURL']);
	}

	/**
	 * Perform other actions when comment cookies are set.
	 *
	 * @since 3.4.0
	 *
	 * @param object $comment Comment object.
	 * @param WP_User $user   User object. The user may not exist.
	 */
	do_action( 'set_comment_cookies', $comment, $user );

	$result = array();
	if ( '0' == $comment->comment_approved ){
		$result['message'] = __('Your comment is awaiting moderation', 'commentator');
	}
	$result['comment_ID'] = $comment_id;

	wp_send_json($result);
    die();
}

function commentator_advise_subscribers($comment_id){
	$comment = get_comment($comment_id);
	$link = get_comment_link( $comment ); 
	$subscribers = get_comment_meta($comment_id, "commentator_subscriptions");
	foreach ($subscribers as $subscriber_email) {
		$from = get_option('admin_email');  
        $headers = __( 'From', 'commentator' ).' '.$from . "</br>";  
        $subject = __( 'A new comment has been posted in a discussion you are following', 'commentator' );
        $msg = __( 'A new comment has been posted in a discussion you are following', 'commentator' ).".<br/><a href='$link'>".__( 'Check it here', 'commentator' )."</a>";
        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        wp_mail( $subscriber_email, $subject, $msg, $headers );
        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	}
	if($comment->comment_parent && $comment->comment_parent > 0){
		commentator_advise_subscribers($comment->comment_parent);
	}
	else{
		commentator_advise_post_subscribers($comment->comment_post_ID);
	}
}

function commentator_advise_post_subscribers($post_id){
	$post = get_post($post_id);
	$link = get_post_permalink($post_id); 
	$subscribers = get_post_meta($post_id, "commentator_subscriptions");
	foreach ($subscribers as $subscriber_email) {
		$from = get_option('admin_email');  
        $headers = __( 'From', 'commentator' ).' '.$from . "</br>";  
        $subject = __( 'A new comment has been posted in a discussion you are following', 'commentator' );
        $msg = __( 'A new comment has been posted in a discussion you are following', 'commentator' ).".<br/><a href='$link'>".__( 'Check it here', 'commentator' )."</a>";
        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        wp_mail( $subscriber_email, $subject, $msg, $headers );
        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	}
}

add_action( 'wp_ajax_commentator_get-comment-html', 'commentator_get_comment_html' );
add_action('wp_ajax_nopriv_commentator_get-comment-html', 'commentator_get_comment_html' );
function commentator_get_comment_html() {
	$comment_id = $_POST['comment_ID'];
	global $comment_depth;
	$comment_depth = get_option('commentator_max_depth', 3);
	$comment = get_comment($comment_id);
	global $post;
	$post = get_post($comment->comment_post_ID);
	commentator_comment( $comment, array(), 0 );
	echo "</li>";
	die();
}

add_action( 'wp_ajax_commentator_vote-comment', 'commentator_vote_comment' );
add_action('wp_ajax_nopriv_commentator_vote-comment', 'commentator_vote_comment' );
function commentator_vote_comment(){
	$multiplicator = $_POST['multiplicator'];
	$voteType = ($multiplicator > 0) ? "upVote" : "downVote";
	$voteOpposite = ($multiplicator < 0) ? "upVote" : "downVote";
	$votes = get_comment_meta( $_POST['comment_ID'], $voteType );

	$errors = array();
	$arr = array();


	global $current_user;
	get_currentuserinfo();

	$voteAuthorID = $current_user->ID;

	$user_profile = isset($_SESSION['commentator_user_profile']) ? $_SESSION["commentator_user_profile"] : null;
	if(!is_user_logged_in() && $user_profile != null){
		$voteAuthorID = $_SESSION["commentator_provider"]."-".$user_profile['identifier'];
	}

	if(!is_user_logged_in() && $user_profile == null){
		$errors[] = __( 'you are not logged in', 'commentator' );
	}

	if(!get_comment($_POST['comment_ID'])){
		$errors[] = __( 'the comment you wan\'t to vote doesn\'t exist', 'commentator' );
	}

	if(count($errors) == 0){
	    if(!in_array ( $voteAuthorID, $votes )){
	    	add_comment_meta( $_POST['comment_ID'], $voteType, $voteAuthorID);
			delete_comment_meta( $_POST['comment_ID'], $voteOpposite, $voteAuthorID );
		}
		else{
			delete_comment_meta( $_POST['comment_ID'], $voteType, $voteAuthorID );
		}


		$votes = get_comment_meta( $_POST['comment_ID'], $voteType );
		$count = count($votes);
		update_comment_meta( $_POST['comment_ID'], $voteType."-count", $count );
		$hasVoted = in_array ( $voteAuthorID, $votes );
		$votesOpposite = get_comment_meta( $_POST['comment_ID'], $voteOpposite );
		$countOpposite = count($votesOpposite);
		update_comment_meta( $_POST['comment_ID'], $voteOpposite."-count", $countOpposite );
		$hasVotedOpposite = in_array ( $voteAuthorID, $votesOpposite );

		$arr = array(
			'count' => $count,
			'hasVoted' => $hasVoted,
			'countOpposite' => $countOpposite,
			'hasVotedOpposite' => $hasVotedOpposite
		);
		$commentarr = array();
		$commentarr['comment_ID'] = $_POST['comment_ID'];
		wp_update_comment( $commentarr );
	}

	else{
		$arr = array(
			'errors' => $errors,
		);
	}


	wp_send_json($arr);
    die();
}

add_action( 'wp_ajax_commentator_sort-comments', 'commentator_sort_comments');
add_action('wp_ajax_nopriv_commentator_sort-comments', 'commentator_sort_comments' );
function commentator_sort_comments(){
	$post_ID = $_POST['comment_post_ID'];
	$args = array(
		'post_id' => $post_ID,
		'order' => 'ASC',
		'status' => 'approve'
	);
	if($_POST['commentator_sort'] == 'picked'){
		$args['comment__in'] = get_post_meta ( $post_ID, "pickedComments");
	}
	
	$comments = get_comments($args);
	$page = null;
	if(get_option('page_comments') && isset($_POST['comment_page'])){
		$page = $_POST['comment_page'];
	}
	if($_POST['commentator_sort'] == 'popular'){
		$comments = commentator_compare_topLevels($comments, 'commentator_comment_karma_comparator');
	}
	else if($_POST['commentator_sort'] == 'desc'){
		$comments = commentator_compare_topLevels($comments, 'commentator_comment_desc_comparator');
	}

	global $post;
	$oldPost = $post;
	$post = get_post($_POST['comment_post_ID']);

    wp_list_comments( 
    	array( 
    		'callback' => 'commentator_comment',
    		'page'              => get_option('page_comments') ? $page : "",
    		'per_page'			=> get_option('page_comments') ? get_option('comments_per_page') : "",
    		'max_depth' => get_option('commentator_max_depth', 3)
    	), $comments);

    ?>
    <?php
    	if(get_option('page_comments')){
    ?>
    <li id="commentator-new-pagination-container">
    	<?php
    	$args = array(
			'base' => '#%#%',
			'add_fragment' => ''
		);
		$max_page = get_comment_pages_count($comments);
		$defaults = array(
			'base' => add_query_arg( 'cpage', '%#%' ),
			'format' => '',
			'total' => $max_page,
			'current' => $page,
			'echo' => true,
			'add_fragment' => '#comments'
		);
		$args = wp_parse_args( $args, $defaults );
		$page_links = paginate_links( $args );

		echo $page_links;
		?>
	</li>
	<?php
    	}
    $post = $oldPost;
    ?>
<?php
	die();
}

function get_comment_karma($comment_id){
	$downVotes = get_comment_meta( $comment_id, "downVote" );
	$downCount = count($downVotes);
	$upVotes = get_comment_meta( $comment_id, "upVote" );
	$upCount = count($upVotes);
	return $upCount - $downCount;
}

function commentator_compare_topLevels($comments, $sorting){
	$firstLevels = array();
	$children = array();
	$result = array();

	foreach ($comments as $index => $comment) {
		if($comment->comment_parent == 0){
			$firstLevels[] = $comment;
		}
		else{
			if(!array_key_exists($comment->comment_parent, $children)){
				$children[$comment->comment_parent] = array();
			}
			$children[$comment->comment_parent][] = $comment;
		}
	}
	
	usort($firstLevels, $sorting);

	foreach($firstLevels as $c){
		$result = commentator_fill_with_children($result, $c, $children);
	}

	return $result;
}

function commentator_fill_with_children($result, $comment, $children){
	$result[] = $comment;
	if(array_key_exists($comment->comment_ID, $children)){
		foreach($children[$comment->comment_ID] as $child){
			$result = commentator_fill_with_children($result, $child, $children);
		}
	}
	return $result;
}

function commentator_comment_karma_comparator($a, $b){
	$compared = 0;
	$karmaA = get_comment_karma($a->comment_ID);
	$karmaB = get_comment_karma($b->comment_ID);
	if($karmaA != $karmaB){
		$compared = ( $karmaA < $karmaB) ? 1:-1;
	}
	return $compared;
}

function commentator_comment_desc_comparator($a, $b){
	$compared = 0;
	if($a->comment_date != $b->comment_date){
		$compared = ( strtotime($a->comment_date) < strtotime($b->comment_date) ) ? 1:-1;
	}
	return $compared;
}



function commentator_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	global $post;
	global $current_user;

	get_currentuserinfo();
	$comment_user = ($comment->user_id && ($comment->user_id > 0)) ? get_user_by( 'id', $comment->user_id  ) : get_user_by( 'email', $comment->comment_author_email );

	$voteAuthorID = $current_user->ID;
	$avatar = get_avatar( $comment );
	$user_profile = isset($_SESSION['commentator_user_profile']) ? $_SESSION["commentator_user_profile"] : null;
	if(!is_user_logged_in() && $user_profile != null){
		$voteAuthorID = $_SESSION["commentator_provider"]."-".$user_profile['identifier'];
	}
	if(get_comment_meta( $comment->comment_ID, "commentator-social-avatar", true )){
		$avatar = '<img alt="" src="'.get_comment_meta( $comment->comment_ID, "commentator-social-avatar", true ).'" class="avatar avatar-96 photo" height="96" width="96">';
	}

	$upVotes = get_comment_meta( $comment->comment_ID, "upVote" );
	$hasUpVoted = in_array ( $voteAuthorID, $upVotes );
	$downVotes = get_comment_meta( $comment->comment_ID, "downVote" );
	$hasDownVoted = in_array ( $voteAuthorID, $downVotes );
	$pickedComments = get_post_meta ( $post->ID, "pickedComments");
	$isPicked = in_array ( $comment->comment_ID, $pickedComments );

	$commentsSubscription = get_comment_meta( $comment->comment_ID, "commentator_subscriptions");
	$isSubscribed = in_array($current_user->user_email, $commentsSubscription );

	$commentUrl = '';
	if(isset($comment_user->ID) && $comment_user->ID > 0){
		if (class_exists('BuddyPress')) {
	        $commentUrl = bp_core_get_user_domain($comment_user->ID);
	    } else if (class_exists('XooUserUltra')) {
	        global $xoouserultra;
	        $commentUrl = $xoouserultra->userpanel->get_user_profile_permalink($comment_user->ID);
	    } else if (class_exists('userpro_api')) {
	        global $userpro;
	        $commentUrl = $userpro->permalink($comment_user->ID);
	    } else if (class_exists('UM_API')) {
	        um_fetch_user($comment_user->ID);
	        $commentUrl = um_user_profile_url();
	    } else {
	        $commentUrl = get_comment_author_url($comment->comment_ID);
	    }
	}

	$authorLink = isset($comment_user->ID) ? $comment_user->display_name : get_comment_author($comment->comment_ID);
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="commentator-comment-content">
			<div class="commentator-avatar hovercard">
				<?php if(empty($commentUrl)){ ?>
					<div class="user">
						<?php echo $avatar; ?>
					</div>
				<?php } else {?>
		            <a href="<?php echo $commentUrl; ?>" class="user">
		                <?php echo $avatar; ?>
		            </a>
		        <?php }?>
	        </div>
	        <div class="commentator-comment-body">
	        	<div class="commentator-comment-header">
                    <span class="commentator-comment-byline">
	                    <span class="author publisher-anchor-color">
	                    	<?php if(empty($commentUrl)){ ?>
								<span class="user">
									<?php echo $authorLink; ?>
								</span>
							<?php } else {?>
					            <a href="<?php echo $commentUrl; ?>" class="user">
					                <?php echo $authorLink; ?>
					            </a>
					        <?php }?>
	                    </span>
                    </span>

                    <?php if ( isset($comment_user->ID) && $comment_user->ID == $post->post_author ){ ?>
                    <span class="commentator-comment-bullet time-ago-bullet">•</span>
                    <span class="commentator-comment-author-tag"><?php _e("author", "commentator"); ?></span>
                   	<?php } ?>

                   	<?php
			        if (class_exists('userpro_api') && isset($comment_user->ID) && $comment_user->ID > 0) {
				        ?>
                    	<span class="commentator-comment-bullet time-ago-bullet">•</span>
				        <span class="commentator-comment-badges">
			                 <?php echo userpro_show_badges($comment_user->ID, $inline = true); ?>
			            </span>
			            <?php
			            }
		            ?>


                    <div class="commentator-comment-meta">
                        <span class="commentator-comment-bullet time-ago-bullet">•</span>
						<?php
							$time = get_option('commentator_time-ago') ? sprintf(__('%1$s ago', 'commentator'), human_time_diff(get_comment_time('U'),current_time('timestamp'))) : sprintf(__( '%1$s at %2$s', 'commentator' ), get_comment_date(),get_comment_time());
							echo '<a href="'.esc_url( get_comment_link( $comment->comment_ID ) ).'" class="commentator-time" title="'.get_comment_time( 'c' ).'">'.$time.'</a>';
						?>
                    </div>

				    <ul class="commentator-comment-menu">
				        <li class="commentator-collapse">
				           <a class="commentator-toggle-visibility" href="#comment-<?php comment_ID(); ?>" title="Collapse">
								<i class="fa fa-minus"></i>
							</a>
				        </li>
				        <li class="commentator-expand">
				            <a class="commentator-toggle-visibility" href="#comment-<?php comment_ID(); ?>" title="Expand">
								<i class="fa fa-plus"></i>
							</a>
				        </li>
			            <?php
			            	$flag = get_option('commentator_flag-comment') && true;
			            	$delete = get_option('commentator_delete-comment') && (isset($comment_user->ID) && $voteAuthorID == $comment_user->ID);
			            	$atLeastTwo = $flag && $delete;

			            	if($atLeastTwo){
			            ?>
			            		<li class="commentator-dropdown">
									<a class="commentator-dropdown-toggle" href="#">
										<i class="fa <?php echo get_option('commentator_icon-comment-dropdown', 'fa-chevron-down'); ?>"></i>
									</a>
									<ul class="commentator-dropdown-menu">
						            <?php
						            	}
						            	if($flag){
						            ?>
						            	<li class="commentator-flag-comment">
											<a href="#" title="<?php _e( 'Flag comment', 'commentator' ); ?>" class="commentator-flag">
												<i class="fa <?php echo get_option('commentator_icon-comment-flag', 'fa-icon-flag'); ?>"></i>
												<span><?php _e( 'Flag comment', 'commentator' ); ?></span>
											</a>
										</li>
									<?php 
										}
										if($delete){
						            ?>
						            	<li class="commentator-delete-comment">
											<a href="#" title="<?php _e( 'Delete comment', 'commentator' ); ?>" class="commentator-delete">
												<i class="fa <?php echo get_option('commentator_icon-comment-delete', 'fa-icon-trash'); ?>"></i>
												<span><?php _e( 'Delete comment', 'commentator' ); ?></span>
											</a>
										</li>
									<?php 
										}
										if($atLeastTwo){
						            ?>
									</ul>
									<?php 
										} 
									?>
				    </ul>
                </div class="commentator-comment-header">
                <?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'commentator' ); ?></p>
				<?php endif; ?>


				<?php
					if(get_option('commentator_allow-images')){
						$image = get_comment_meta( $comment->comment_ID, "commentator-attached-image", true);
						if($image){
				?>

					<a class="commentator-attached-image" href="<?php echo $image; ?>" target="_blank">
						<img src="<?php echo $image; ?>"/>
					</a>

				<?php
						}
					}
				?>

				<section class="commentator-comment-text">

				<?php
					$comment_text = get_comment_text();
					if(get_option('commentator_enable-oembed')){
						global $wp_embed;
						$comment_text = $wp_embed->autoembed($comment_text);
					}
					if(get_option('commentator_enable-clickable-links')){
						$comment_text = make_clickable( $comment_text );
					}
					echo $comment_text;
				?>
				</section><!-- .comment-content -->

				<div class="commentator-comment-footer">
					<?php
						if(get_option('commentator_pick-comment')){
					?>
					<a href="#" class="commentator-pick<?php if($isPicked){ echo " commentator-active"; } if($voteAuthorID != $post->post_author){ echo " commentator-disabled"; } ?>" title="<?php if($isPicked){_e( 'Unpick comment', 'commentator' );}else{_e( 'Pick comment', 'commentator' );} ?>">
						<i class="fa <?php echo get_option('commentator_icon-comment-pick', 'fa-thumb-tack'); ?>"></i>
					</a>
		            <span class="commentator-comment-bullet">•</span>
					<?php
						}
					?>
		            <span class="commentator-voting">
					    <a href="#" class="commentator-vote-up<?php if($hasUpVoted){ echo " commentator-active"; } ?>" title="<?php _e( 'Vote up', 'commentator' ); ?>" data-comment-id="<?php comment_ID(); ?>">
					        <span><?php echo count($upVotes); ?></span>
					        <i class="fa <?php echo get_option('commentator_icon-comment-upvote', 'fa-caret-up'); ?>"></i>
					    </a>
					    <a href="#" class="commentator-vote-down<?php if($hasDownVoted){ echo " commentator-active"; } ?>" title="<?php _e( 'Vote down', 'commentator' ); ?>" data-comment-id="<?php comment_ID(); ?>">
					        <span><?php echo count($downVotes); ?></span>
					        <i class="fa <?php echo get_option('commentator_icon-comment-downvote', 'fa-caret-down'); ?>"></i>
					    </a>
		            </span>
		            <?php global $comment_depth; ?>
		            <?php
		            	if($comment_depth < get_option('commentator_max_depth', 3)){
		            ?>
		            <span class="commentator-comment-bullet">•</span>
					<span>
						<a href="#" class="commentator-reply">
							<i class="fa <?php echo get_option('commentator_icon-comment-reply', 'fa-reply'); ?>"></i>
							<span><?php _e( 'Reply', 'commentator' ); ?></span>
						</a>
					</span>
					<?php } ?>
					<?php
		            	if(get_option('commentator_subscribe')){
		            ?>
		            <span class="commentator-comment-bullet">•</span>
					<span>
						<a href="#" class="commentator-subscribe<?php if($isSubscribed){ echo " commentator-active"; }?>" title="<?php if($isSubscribed){_e( 'Unsubscribe from comment', 'commentator' );}else{_e( 'Subscribe to comment', 'commentator' );} ?>" data-type="comment_ID" data-id="<?php comment_ID(); ?>">
							<i class="fa <?php echo get_option('commentator_icon-subscribe', 'fa-envelope'); ?>"></i>
						</a>
					</span>
					<?php } ?>
			    </div>
	        </div>
		</article><!-- #comment-## -->
	<?php
}

function commentator_atLeastTwo($a, $b, $c) {
    return $a ? ($b || $c) : ($b && $c);
}


function commentator_shortcode( $atts ){
	ob_start();
	comments_template();
	return ob_get_clean();
}
add_shortcode( 'commentator', 'commentator_shortcode' );

/**
 * Proper way to enqueue scripts and styles
**/

function commentator_scripts() {
	$dependencies = array( 'jquery' );
	if(get_option('commentator_allow-images')){
		wp_enqueue_script( 'jquery.iframe-transport', plugins_url( '/js/jquery.iframe-transport.js', __FILE__ ), array( 'jquery' ), true);
		wp_enqueue_script( 'jquery.ui.widget', plugins_url( '/js/jquery.ui.widget.js', __FILE__ ), array( 'jquery' ), true);
		wp_enqueue_script( 'jquery.fileupload', plugins_url( '/js/jquery.fileupload.js', __FILE__ ), array( 'jquery', 'jquery.iframe-transport', 'jquery.ui.widget' ), true);
		$dependencies[]	= 'jquery.fileupload';
	}

	if(get_option('commentator_enable-oembed')){
		wp_enqueue_script( 'jquery.fitvids', plugins_url( '/js/jquery.fitvids.js', __FILE__ ), array( 'jquery' ), true);
		$dependencies[]	= 'jquery.fitvids';
	}
	
	wp_enqueue_script( 'commentator-script', plugins_url( '/js/commentator-script.js', __FILE__ ), $dependencies, '2.6.3', true);
	wp_enqueue_style( 'commentator-style', plugins_url( '/css/commentator.css', __FILE__ ), '2.6.2' );

	if(!get_option('commentator_fontawesome-loaded')){
		wp_enqueue_style( 'commentator-font-awesome', plugins_url( '/css/font-awesome.min.css', __FILE__), '4.5.0' );
	}
}
if(!get_option('commentator_load-from-cdn')) {
	add_action( 'wp_enqueue_scripts', 'commentator_scripts', 15 );
}

function commentator_admin_scripts($hook) {
	global $commentator_settings_page;
 
	if( $hook != $commentator_settings_page ) 
		return;

	wp_enqueue_script( 'commentator-admin-script', plugins_url( '/js/commentator-admin-script.js', __FILE__ ), array( 'jquery' ), '1.0');
	wp_enqueue_style( 'commentator-admin-style', plugins_url( '/css/commentator-admin.css', __FILE__ ) );

	wp_enqueue_script( 'commentator-color-picker-script', plugins_url( '/colorpicker/js/colorpicker.js', __FILE__ ), array( 'jquery' ), '1.0');
	wp_enqueue_style( 'commentator-color-picker-style', plugins_url( '/colorpicker/css/colorpicker.css', __FILE__ ) );

	wp_enqueue_script( 'commentator-select2', plugins_url( '/select2/select2.js', __FILE__ ), array( 'jquery' ), '1.0');
	wp_enqueue_style( 'commentator-select2-style', plugins_url( '/select2/select2.css', __FILE__ ) );

	wp_enqueue_style( 'commentator-font-awesome', plugins_url( '/css/font-awesome.min.css', __FILE__ ) );
}
add_action( 'admin_enqueue_scripts', 'commentator_admin_scripts' );

add_filter('comments_template', 'commentator_template');
function commentator_template($passed){
	return dirname(__FILE__) . '/php/commentator-template.php';
}

function commentator_comment_columns( $columns )
{
	return array_merge( $columns, array(
		'upVote-count' => __( 'Upvotes', 'commentator' ),
		'downVote-count' => __( 'Downvotes', 'commentator' ),
		'flag-comment-count' => __( 'Flags', 'commentator' )
	) );
}
add_filter( 'manage_edit-comments_columns', 'commentator_comment_columns' );

function commentator_comment_column( $column, $comment_ID )
{
	if ( $meta = get_comment_meta( $comment_ID, $column , true ) ) {
		echo $meta;
	} else {
		echo '-';
	}
}
add_filter( 'manage_comments_custom_column', 'commentator_comment_column', 10, 2 );

// Register the column as sortable
function register_sortable_columns( $columns ) {
    return array_merge( $columns, array(
		'upVote-count' => 'upVote-count',
		'downVote-count' => 'downVote-count',
		'flag-comment-count' => 'flag-comment-count'
	) );
}
add_filter( 'manage_edit-comments_sortable_columns', 'register_sortable_columns' );

add_action( 'pre_get_comments', 'commentator_orderby' );  
function commentator_orderby( $comments ) {  
    $orderby = $comments->query_vars['orderby'];
    if( 'upVote-count' == $orderby || 'downVote-count' == $orderby || 'flag-comment-count' == $orderby ) {  
        $comments->query_vars['meta_key'] = $orderby;
        $comments->query_vars['orderby'] = 'meta_value_num';
    }
    $comments->meta_query->parse_query_vars( $comments->query_vars );
}

global $arrayProviders;
$arrayProviders = array(
	array(
		"Google",
		"google",
		"google-plus"
	),
	array(
		"LinkedIn",
		"linkedin",
		"linkedin"
	),
	array(
		"Facebook",
		"facebook",
		"facebook"
	),
	array(
		"Twitter",
		"twitter",
		"twitter"
	)
);

function register_commentator_settings(){
	global $arrayProviders;
	//register our settings
	register_setting( 'commentator-settings-group', 'commentator_max_depth' );
	register_setting( 'commentator-settings-group', 'commentator_anybody_register' );
	register_setting( 'commentator-settings-group', 'commentator_register_password_chose' );
	register_setting( 'commentator-settings-group', 'commentator_disable-login-tab' );
	register_setting( 'commentator-settings-group', 'commentator_before' );
	register_setting( 'commentator-settings-group', 'commentator_custom-menu' );
	register_setting( 'commentator-settings-group', 'commentator_disable-avatars' );
	register_setting( 'commentator-settings-group', 'commentator_disable-thread-votes' );
	register_setting( 'commentator-settings-group', 'commentator_allow-images' );
	register_setting( 'commentator-settings-group', 'commentator_enable-oembed' );
	register_setting( 'commentator-settings-group', 'commentator_enable-clickable-links' );
	register_setting( 'commentator-settings-group', 'commentator_order-tabs' );
	register_setting( 'commentator-settings-group', 'commentator_flag-comment' );
	register_setting( 'commentator-settings-group', 'commentator_pick-comment' );
	register_setting( 'commentator-settings-group', 'commentator_subscribe' );
	register_setting( 'commentator-settings-group', 'commentator_delete-comment' );
	register_setting( 'commentator-settings-group', 'commentator_flag-limit');
	register_setting( 'commentator-settings-group', 'commentator_user-roles' );
	register_setting( 'commentator-settings-group', 'commentator_time-ago');
	register_setting( 'commentator-settings-group', 'commentator_disabled-user-roles');


	register_setting( 'commentator-settings-group', 'commentator_updated-counts-in-db' );
	if(!get_option( 'commentator_updated-counts-in-db' )){
		update_option( 'commentator_updated-counts-in-db', '1' );
		$arrayComments = get_comments();
		foreach ($arrayComments as $comment) {
			$comment_ID = $comment->comment_ID;

			$countFlags = count(get_comment_meta($comment_ID, "flag-comment"));
	    	update_comment_meta($comment_ID, "flag-comment-count", $countFlags);

			$countDownVotes = count(get_comment_meta( $comment_ID, "downVote" ));
			update_comment_meta( $comment_ID, "downVote-count", $countDownVotes );

			$countUpVotes = count(get_comment_meta( $comment_ID, "upVote" ));
			update_comment_meta( $comment_ID, "upVote-count", $countUpVotes );
		}
	}
	register_setting( 'commentator-settings-group', 'commentator_base-theme' );

	register_setting( 'commentator-settings-group', 'commentator_icon-thread-vote' );
	register_setting( 'commentator-settings-group', 'commentator_color-thread-vote' );
	register_setting( 'commentator-settings-group', 'commentator_icon-thread-voted' );
	register_setting( 'commentator-settings-group', 'commentator_color-thread-voted' );
	register_setting( 'commentator-settings-group', 'commentator_color-up-voted' );
	register_setting( 'commentator-settings-group', 'commentator_color-down-voted' );
	register_setting( 'commentator-settings-group', 'commentator_color-picked' );
	register_setting( 'commentator-settings-group', 'commentator_icon-send-message' );
	register_setting( 'commentator-settings-group', 'commentator_icon-subscribe' );
	register_setting( 'commentator-settings-group', 'commentator_color-subscribe' );
	register_setting( 'commentator-settings-group', 'commentator_icon-comment-upvote' );
	register_setting( 'commentator-settings-group', 'commentator_icon-comment-downvote' );
	register_setting( 'commentator-settings-group', 'commentator_icon-comment-reply' );
	register_setting( 'commentator-settings-group', 'commentator_icon-register' );
	register_setting( 'commentator-settings-group', 'commentator_icon-login' );
	register_setting( 'commentator-settings-group', 'commentator_icon-comment-flag' );
	register_setting( 'commentator-settings-group', 'commentator_icon-comment-pick' );
	register_setting( 'commentator-settings-group', 'commentator_icon-comment-delete' );

	register_setting( 'commentator-settings-group', 'commentator_social-signin' );
	foreach ($arrayProviders as $provider) {
		register_setting( 'commentator-settings-group', 'commentator_id_key-'.$provider[1] );
		register_setting( 'commentator-settings-group', 'commentator_secret_key-'.$provider[1] );
	}

	register_setting( 'commentator-settings-group', 'commentator_load-from-cdn' );
	register_setting( 'commentator-settings-group', 'commentator_fontawesome-loaded' );

	$all_options = wp_load_alloptions();
	foreach( $all_options as $name => $value ) {
		if(substr( $name, 0, 16 ) === "commentator_icon" && substr( $value, 0, 16 ) === "commentator-icon"){
			$new_value = str_replace("commentator-icon", "fa", $value);
			update_option( $name, $new_value );
		}
	}

}

function commentator_add_pages() {
	global $commentator_settings_page;
	$commentator_settings_page = add_comments_page(__( 'Overview for the Commentator Plugin', 'commentator' ), __( 'Commentator', 'commentator' ), 'manage_options', 'commentator_overview', 'commentator_overview');

}

function icon_option($title, $name, $defaultIcon, $defaultColor){
?>
	<tr>
        <th scope="row"><?php echo $title; ?></th>
        <td>
            <?php icon_choose_option($name, $defaultIcon); ?>
        	<?php color_option($name, $defaultColor); ?>
        </td>
    </tr>
<?php
}

function icon_only_option($title, $name, $defaultIcon){
?>
	<tr>
        <th scope="row"><?php echo $title; ?></th>
        <td>
            <?php icon_choose_option($name, $defaultIcon); ?>
        </td>
    </tr>
<?php
}

function color_only_option($title, $name, $defaultColor){
?>
	<tr>
        <th scope="row"><?php echo $title; ?></th>
        <td>
            <?php color_option($name, $defaultColor); ?>
        </td>
    </tr>
<?php
}

function icon_choose_option($name, $defaultIcon){
?>
	<div class="commentator-icon-preview">
		<i class="fa commentator-icon-modal <?php echo get_option('commentator_icon-'.$name, $defaultIcon); ?>"></i>
	</div>
	<input class="commentator-icon-input" type="hidden" name="commentator_icon-<?php echo $name; ?>" value="<?php echo get_option('commentator_icon-'.$name, $defaultIcon); ?>" data-commentator-default="<?php echo $defaultIcon; ?>" />
<?php
}

function color_option($name, $defaultColor){
?>
	<div class="commentator-color-preview">
		<div style="background-color: <?php echo get_option('commentator_color-'.$name, $defaultColor); ?>;">
		</div>
	</div>
	<input class="commentator-color-input" type="hidden" name="commentator_color-<?php echo $name; ?>" value="<?php echo get_option('commentator_color-'.$name, $defaultColor); ?>" data-commentator-default="<?php echo $defaultColor; ?>"/>
<?php
}

function social_provider_option($provider_array){
	$provider = $provider_array[0];
	$provider_perma = $provider_array[1];
	$provider_icon = $provider_array[2];
	?>
		<tr>
	        <th scope="row"><?php echo $provider; ?></th>
	        <td class="commentator-form-icon-social">
	        	<i class="fa fa-<?php echo $provider_icon; ?>"></i>
	        </td>
	        <td>
	            <label>ID Key</label>
				<input type="text" name="commentator_id_key-<?php echo $provider_perma; ?>" value="<?php echo get_option('commentator_id_key-'.$provider_perma); ?>" data-commentator-default=""/>
				<label>Secret Key</label>
				<input type="text" name="commentator_secret_key-<?php echo $provider_perma; ?>" value="<?php echo get_option('commentator_secret_key-'.$provider_perma); ?>" data-commentator-default=""/>
	        </td>
	    </tr>
	<?php
}

function get_tab_name($key){
	$result = array(
		"asc" => __("Oldest", "commentator"),
		"desc" => __("Newest", "commentator"),
		"popular" => __("Best", "commentator"),
		"picked" => __("Picked", "commentator"),
	);
	return $result[$key];
}

function commentator_overview() {
	global $arrayProviders;
?>
<div id="commentator-admin-settings">
	<div class="wrap">
		<h2>Commentator Overview</h2>
	    <ul class="commentator-admin-tabs">
	    	<li>
	    		<a href="#commentator-admin-general" class="commentator-active"><?php _e( 'General', 'commentator' ); ?></a>
	    	</li>
	    	<li>
	    		<a href="#commentator-admin-icons"><?php _e( 'Icons', 'commentator' ); ?></a>
	    	</li>
	    	<li>
	    		<a href="#commentator-admin-style"><?php _e( 'Style', 'commentator' ); ?></a>
	    	</li>
	    	<li>
	    		<a href="#commentator-admin-social"><?php _e( 'Social', 'commentator' ); ?></a>
	    	</li>
	    	<li>
	    		<a href="#commentator-admin-advanced"><?php _e( 'Advanced', 'commentator' ); ?></a>
	    	</li>
	    </ul>
		<form method="post" action="options.php">
		    <?php settings_fields( 'commentator-settings-group' ); ?>
		    <?php do_settings_sections( 'commentator-settings-group' ); ?>

			<div class="commentator-admin-wrapper">
			    <div class="commentator-tabs-content">
			    	<div id="commentator-admin-general" class="commentator-tab-content commentator-active">
					    <table class="form-table">
					        <tr>
						        <th scope="row"><?php _e( 'Max Threaded Comments Depth (default : 3)', 'commentator' ); ?></th>
						        <td><input type="text" name="commentator_max_depth" value="<?php echo get_option('commentator_max_depth', 3); ?>" data-commentator-default="3"/></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Anybody can register ?', 'commentator' ); ?></th>
						        <td><input name="commentator_anybody_register" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_anybody_register' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Comment subscription', 'commentator' ); ?></th>
						        <td><input name="commentator_subscribe" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_subscribe' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Human readable time (ago) ?', 'commentator' ); ?></th>
						        <td><input name="commentator_time-ago" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_time-ago' ) ); ?> /></td>
					        </tr>
					        <tr>
					        	 <th scope="row"><?php _e( 'Chose your password when registering ?', 'commentator' ); ?></th>
						        <td><input name="commentator_register_password_chose" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_register_password_chose' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Disable login tab ?', 'commentator' ); ?></th>
						        <td><input name="commentator_disable-login-tab" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_disable-login-tab' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Enable comment flagging ?', 'commentator' ); ?></th>
						        <td><input name="commentator_flag-comment" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_flag-comment' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Enable comment deletion by their author ?', 'commentator' ); ?></th>
						        <td><input name="commentator_delete-comment" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_delete-comment' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Enable comment picking ?', 'commentator' ); ?></th>
						        <td><input name="commentator_pick-comment" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_pick-comment' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'From how many flags untill a comment gets unapproved (leave blank for no automation)', 'commentator' ); ?></th>
						        <td><input type="text" name="commentator_flag-limit" value="<?php echo get_option('commentator_flag-limit'); ?>"/></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Disable thread votes ?', 'commentator' ); ?></th>
						        <td><input name="commentator_disable-thread-votes" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_disable-thread-votes' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Section of content before the commenting section', 'commentator' ); ?></th>
						        <td><textarea name="commentator_before"><?php echo get_option('commentator_before', '') ?></textarea></td>
					        </tr>

					        <tr>
						        <th scope="row"><?php _e( 'Custom menu html', 'commentator' ); ?></th>
						        <td><textarea name="commentator_custom-menu"><?php echo get_option('commentator_custom-menu', '') ?></textarea></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Disable avatars ?', 'commentator' ); ?></th>
						        <td><input name="commentator_disable-avatars" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_disable-avatars' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Enable images uploads ?', 'commentator' ); ?></th>
						        <td><input name="commentator_allow-images" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_allow-images' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Enable oEmbed ?', 'commentator' ); ?></th>
						        <td><input name="commentator_enable-oembed" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_enable-oembed' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Enable clickable links ?', 'commentator' ); ?></th>
						        <td><input name="commentator_enable-clickable-links" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_enable-clickable-links' ) ); ?> /></td>
					        </tr>
					        <tr>
					        	<?php
					        		$tabs = get_option( 'commentator_order-tabs', 'picked,popular,asc,desc|');
					        		$arrayTabs = explode( "|", $tabs );
					        		$enabledTabs = explode( ",", $arrayTabs[0] );
					        		$disabledTabs = explode( ",", $arrayTabs[1] );
					        	?>
						        <th scope="row"><?php _e( 'Order and Disable Tabs ?', 'commentator' ); ?></th>
						        <td>
						        	<span><?php _e( 'Enabled Tabs', 'commentator' ); ?></span>
						        	<div class="commentator-tab-sortable" id="commentator-enabled-tabs">
						        		<?php
						        			foreach ($enabledTabs as $value) {
						        				if(!empty($value)){
											    ?>
											    <div class="commentator-tab" data-commentator-tab="<?php echo $value; ?>">
											    	<span><?php echo get_tab_name($value); ?></span>
											    </div>
											    <?php
												}
											}
						        		?>
						        	</div>
						          	<input id="commentator_order-tabs" name="commentator_order-tabs" type="hidden" value="<?php echo get_option( 'commentator_order-tabs', 'picked,popular,asc,desc|'); ?>">
						        </td>
						        <td>
						        	<span><?php _e( 'Disabled Tabs', 'commentator' ); ?></span>
						        	<div class="commentator-tab-sortable" id="commentator-disabled-tabs">
						        		<?php
						        			foreach ($disabledTabs as $value) {
						        				if(!empty($value)){
											    ?>
											    <div class="commentator-tab" data-commentator-tab="<?php echo $value; ?>">
											    	<span><?php echo get_tab_name($value); ?></span>
											    </div>
											    <?php
												}
											}
						        		?>
						        	</div>
						        </td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'Disable commenting abilities to some user roles ?', 'commentator' ); ?></th>
						        <td colspan="2">
						        	<?php
											// create a new role for Members
											$roles = get_option('commentator_disabled-user-roles', array());
											$roles = is_array($roles) ? $roles : array();
											global $wp_roles;
    										$allRoles = $wp_roles->get_names();
    								?>
						        	<select class="commentator-select2" name="commentator_disabled-user-roles[]" multiple style="width:100%;">
						        			<?php
						        		
											foreach ($allRoles as $role_name => $role_info){
												$selected = in_array ( $role_name, $roles ) ? " selected" : "";
											?>
												<option value="<?php echo $role_name; ?>"<?php echo $selected; ?>><?php echo $role_info; ?></option>
											<?php
											}
						        		?>
						        	</select>
						        </td>
					        </tr>
					    </table>
					</div>
					<div id="commentator-admin-style" class="commentator-tab-content">
					    <table class="form-table">
					    	<tr>
						        <th scope="row"><?php _e( 'Base Theme', 'commentator' ); ?></th>
						        <td>
						        	<?php
											// create a new role for Members
											$selectedTheme = get_option('commentator_base-theme', 'light');
    										$allThemes = array('light', 'dark', 'boxed');
    								?>
						        	<select class="commentator-select2" name="commentator_base-theme" style="width:100%;">
						        			<?php
						        		
											foreach ($allThemes as $theme){
												$selected = ($selectedTheme == $theme) ? " selected" : "";
											?>
												<option value="<?php echo $theme; ?>"<?php echo $selected; ?>><?php echo $theme; ?></option>
											<?php
											}
						        		?>
						        	</select>
						        </td>
					        </tr>
					    </table>
					</div>
					<div id="commentator-admin-icons" class="commentator-tab-content">
					    <table class="form-table">
					        <?php icon_option(__( 'Icon and color for thread voting', 'commentator' ), 'thread-vote', 'fa-star', '#ffbf00'); ?>
					        <?php icon_only_option(__( 'Icon for thread voted', 'commentator' ), 'thread-voted', 'fa-check'); ?>
					        <?php color_only_option(__( 'Color when the thread is voted', 'commentator' ), 'thread-voted', '#8fc847'); ?>
					        <?php icon_only_option(__( 'Icon for comment button', 'commentator' ), 'send-message', 'fa-rocket'); ?>
					        <?php icon_only_option(__( 'Icon for comment upvote', 'commentator' ), 'comment-upvote', 'fa-caret-up'); ?>
					        <?php icon_only_option(__( 'Icon for comment downvote', 'commentator' ), 'comment-downvote', 'fa-chevron-down'); ?>
					        <?php icon_only_option(__( 'Icon for comment flagging', 'commentator' ), 'comment-flag', 'fa-flag'); ?>
					        <?php icon_only_option(__( 'Icon to pick a comment', 'commentator' ), 'comment-pick', 'fa-thumb-tack'); ?>
					        <?php icon_only_option(__( 'Icon to delete a comment', 'commentator' ), 'comment-delete', 'fa-trash'); ?>
					        <?php icon_only_option(__( 'Icon for comment reply', 'commentator' ), 'comment-reply', 'fa-reply'); ?>
					        <?php icon_only_option(__( 'Icon for comment register', 'commentator' ), 'register', 'fa-arrow-right'); ?>
					        <?php icon_only_option(__( 'Icon for comment login', 'commentator' ), 'login', 'fa-arrow-right'); ?>
					        <?php icon_option(__( 'Icon for comment subscription', 'commentator' ), 'subscribe', 'fa-envelope', '#8fc847'); ?>
					        <?php color_only_option(__( 'Color when a comment is down voted', 'commentator' ), 'down-voted', '#f55567'); ?>
					        <?php color_only_option(__( 'Color when a comment is up voted', 'commentator' ), 'up-voted', '#2e9fff'); ?>
					        <?php color_only_option(__( 'Color when a comment is picked', 'commentator' ), 'picked', '#8fc847'); ?>
					    </table>
					</div>
					<div id="commentator-admin-social" class="commentator-tab-content">
					    <table class="form-table">
					        <tr>
						        <th scope="row"><?php _e( 'Enable Social Signin ?', 'commentator' ); ?></th>
						        <td><input name="commentator_social-signin" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_social-signin' ) ); ?> /></td>
					        </tr>
					        <?php 
					        	foreach ($arrayProviders as $provider) {
									social_provider_option($provider);
								}
							?>
					    </table>
					</div>
					<div id="commentator-admin-advanced" class="commentator-tab-content">
					    <table class="form-table">
					    	<tr>
						        <th scope="row"><?php _e( 'I load my scripts myself from my CDN', 'commentator' ); ?></th>
						        <td><input name="commentator_load-from-cdn" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_load-from-cdn' ) ); ?> /></td>
					        </tr>
					        <tr>
						        <th scope="row"><?php _e( 'I already have fontawesome installed on my website', 'commentator' ); ?></th>
						        <td><input name="commentator_fontawesome-loaded" type="checkbox" value="1" <?php checked( '1', get_option( 'commentator_fontawesome-loaded' ) ); ?> /></td>
					        </tr>
					    </table>
					</div>
				</div>
			</div>
			<div class="commentator-admin-footer"><!-- 
		    	<a href="#" class="commentator-reset-default">Reset all to defaults</a>    --> 
		    	<?php submit_button( 'Submit', 'primary', 'submit-form', false ); ?>
		    </div>
		</form>
	</div>
	<div id="commentator-modal-overlay">
		<section id="commentator-icons">
			<h3><?php _e( 'Choose an icon', 'commentator' ); ?></h3>
			<div id="commentator-icon-list">
				<i class="fa fa-glass"></i>
				<i class="fa fa-music"></i>
				<i class="fa fa-search"></i>
				<i class="fa fa-envelope-o"></i>
				<i class="fa fa-heart"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-user"></i>
				<i class="fa fa-film"></i>
				<i class="fa fa-th-large"></i>
				<i class="fa fa-th"></i>
				<i class="fa fa-th-list"></i>
				<i class="fa fa-check"></i>
				<i class="fa fa-remov"></i>
				<i class="fa fa-clos"></i>
				<i class="fa fa-times"></i>
				<i class="fa fa-search-plus"></i>
				<i class="fa fa-search-minus"></i>
				<i class="fa fa-power-off"></i>
				<i class="fa fa-signal"></i>
				<i class="fa fa-gea"></i>
				<i class="fa fa-cog"></i>
				<i class="fa fa-trash-o"></i>
				<i class="fa fa-home"></i>
				<i class="fa fa-file-o"></i>
				<i class="fa fa-clock-o"></i>
				<i class="fa fa-road"></i>
				<i class="fa fa-download"></i>
				<i class="fa fa-arrow-circle-o-down"></i>
				<i class="fa fa-arrow-circle-o-up"></i>
				<i class="fa fa-inbox"></i>
				<i class="fa fa-play-circle-o"></i>
				<i class="fa fa-rotate-righ"></i>
				<i class="fa fa-repeat"></i>
				<i class="fa fa-refresh"></i>
				<i class="fa fa-list-alt"></i>
				<i class="fa fa-lock"></i>
				<i class="fa fa-flag"></i>
				<i class="fa fa-headphones"></i>
				<i class="fa fa-volume-off"></i>
				<i class="fa fa-volume-down"></i>
				<i class="fa fa-volume-up"></i>
				<i class="fa fa-qrcode"></i>
				<i class="fa fa-barcode"></i>
				<i class="fa fa-tag"></i>
				<i class="fa fa-tags"></i>
				<i class="fa fa-book"></i>
				<i class="fa fa-bookmark"></i>
				<i class="fa fa-print"></i>
				<i class="fa fa-camera"></i>
				<i class="fa fa-font"></i>
				<i class="fa fa-bold"></i>
				<i class="fa fa-italic"></i>
				<i class="fa fa-text-height"></i>
				<i class="fa fa-text-width"></i>
				<i class="fa fa-align-left"></i>
				<i class="fa fa-align-center"></i>
				<i class="fa fa-align-right"></i>
				<i class="fa fa-align-justify"></i>
				<i class="fa fa-list"></i>
				<i class="fa fa-deden"></i>
				<i class="fa fa-outdent"></i>
				<i class="fa fa-indent"></i>
				<i class="fa fa-video-camera"></i>
				<i class="fa fa-phot"></i>
				<i class="fa fa-imag"></i>
				<i class="fa fa-picture-o"></i>
				<i class="fa fa-pencil"></i>
				<i class="fa fa-map-marker"></i>
				<i class="fa fa-adjust"></i>
				<i class="fa fa-tint"></i>
				<i class="fa fa-edi"></i>
				<i class="fa fa-pencil-square-o"></i>
				<i class="fa fa-share-square-o"></i>
				<i class="fa fa-check-square-o"></i>
				<i class="fa fa-arrows"></i>
				<i class="fa fa-step-backward"></i>
				<i class="fa fa-fast-backward"></i>
				<i class="fa fa-backward"></i>
				<i class="fa fa-play"></i>
				<i class="fa fa-pause"></i>
				<i class="fa fa-stop"></i>
				<i class="fa fa-forward"></i>
				<i class="fa fa-fast-forward"></i>
				<i class="fa fa-step-forward"></i>
				<i class="fa fa-eject"></i>
				<i class="fa fa-chevron-left"></i>
				<i class="fa fa-chevron-right"></i>
				<i class="fa fa-plus-circle"></i>
				<i class="fa fa-minus-circle"></i>
				<i class="fa fa-times-circle"></i>
				<i class="fa fa-check-circle"></i>
				<i class="fa fa-question-circle"></i>
				<i class="fa fa-info-circle"></i>
				<i class="fa fa-crosshairs"></i>
				<i class="fa fa-times-circle-o"></i>
				<i class="fa fa-check-circle-o"></i>
				<i class="fa fa-ban"></i>
				<i class="fa fa-arrow-left"></i>
				<i class="fa fa-arrow-right"></i>
				<i class="fa fa-arrow-up"></i>
				<i class="fa fa-arrow-down"></i>
				<i class="fa fa-mail-forwar"></i>
				<i class="fa fa-share"></i>
				<i class="fa fa-expand"></i>
				<i class="fa fa-compress"></i>
				<i class="fa fa-plus"></i>
				<i class="fa fa-minus"></i>
				<i class="fa fa-asterisk"></i>
				<i class="fa fa-exclamation-circle"></i>
				<i class="fa fa-gift"></i>
				<i class="fa fa-leaf"></i>
				<i class="fa fa-fire"></i>
				<i class="fa fa-eye"></i>
				<i class="fa fa-eye-slash"></i>
				<i class="fa fa-warnin"></i>
				<i class="fa fa-exclamation-triangle"></i>
				<i class="fa fa-plane"></i>
				<i class="fa fa-calendar"></i>
				<i class="fa fa-random"></i>
				<i class="fa fa-comment"></i>
				<i class="fa fa-magnet"></i>
				<i class="fa fa-chevron-up"></i>
				<i class="fa fa-chevron-down"></i>
				<i class="fa fa-retweet"></i>
				<i class="fa fa-shopping-cart"></i>
				<i class="fa fa-folder"></i>
				<i class="fa fa-folder-open"></i>
				<i class="fa fa-arrows-v"></i>
				<i class="fa fa-arrows-h"></i>
				<i class="fa fa-bar-chart-"></i>
				<i class="fa fa-bar-chart"></i>
				<i class="fa fa-twitter-square"></i>
				<i class="fa fa-facebook-square"></i>
				<i class="fa fa-camera-retro"></i>
				<i class="fa fa-key"></i>
				<i class="fa fa-gear"></i>
				<i class="fa fa-cogs"></i>
				<i class="fa fa-comments"></i>
				<i class="fa fa-thumbs-o-up"></i>
				<i class="fa fa-thumbs-o-down"></i>
				<i class="fa fa-star-half"></i>
				<i class="fa fa-heart-o"></i>
				<i class="fa fa-sign-out"></i>
				<i class="fa fa-linkedin-square"></i>
				<i class="fa fa-thumb-tack"></i>
				<i class="fa fa-external-link"></i>
				<i class="fa fa-sign-in"></i>
				<i class="fa fa-trophy"></i>
				<i class="fa fa-github-square"></i>
				<i class="fa fa-upload"></i>
				<i class="fa fa-lemon-o"></i>
				<i class="fa fa-phone"></i>
				<i class="fa fa-square-o"></i>
				<i class="fa fa-bookmark-o"></i>
				<i class="fa fa-phone-square"></i>
				<i class="fa fa-twitter"></i>
				<i class="fa fa-facebook-"></i>
				<i class="fa fa-facebook"></i>
				<i class="fa fa-github"></i>
				<i class="fa fa-unlock"></i>
				<i class="fa fa-credit-card"></i>
				<i class="fa fa-fee"></i>
				<i class="fa fa-rss"></i>
				<i class="fa fa-hdd-o"></i>
				<i class="fa fa-bullhorn"></i>
				<i class="fa fa-bell"></i>
				<i class="fa fa-certificate"></i>
				<i class="fa fa-hand-o-right"></i>
				<i class="fa fa-hand-o-left"></i>
				<i class="fa fa-hand-o-up"></i>
				<i class="fa fa-hand-o-down"></i>
				<i class="fa fa-arrow-circle-left"></i>
				<i class="fa fa-arrow-circle-right"></i>
				<i class="fa fa-arrow-circle-up"></i>
				<i class="fa fa-arrow-circle-down"></i>
				<i class="fa fa-globe"></i>
				<i class="fa fa-wrench"></i>
				<i class="fa fa-tasks"></i>
				<i class="fa fa-filter"></i>
				<i class="fa fa-briefcase"></i>
				<i class="fa fa-arrows-alt"></i>
				<i class="fa fa-grou"></i>
				<i class="fa fa-users"></i>
				<i class="fa fa-chai"></i>
				<i class="fa fa-link"></i>
				<i class="fa fa-cloud"></i>
				<i class="fa fa-flask"></i>
				<i class="fa fa-cu"></i>
				<i class="fa fa-scissors"></i>
				<i class="fa fa-cop"></i>
				<i class="fa fa-files-o"></i>
				<i class="fa fa-paperclip"></i>
				<i class="fa fa-sav"></i>
				<i class="fa fa-floppy-o"></i>
				<i class="fa fa-square"></i>
				<i class="fa fa-navico"></i>
				<i class="fa fa-reorde"></i>
				<i class="fa fa-bars"></i>
				<i class="fa fa-list-ul"></i>
				<i class="fa fa-list-ol"></i>
				<i class="fa fa-strikethrough"></i>
				<i class="fa fa-underline"></i>
				<i class="fa fa-table"></i>
				<i class="fa fa-magic"></i>
				<i class="fa fa-truck"></i>
				<i class="fa fa-pinterest"></i>
				<i class="fa fa-pinterest-square"></i>
				<i class="fa fa-google-plus-square"></i>
				<i class="fa fa-google-plus"></i>
				<i class="fa fa-money"></i>
				<i class="fa fa-caret-down"></i>
				<i class="fa fa-caret-up"></i>
				<i class="fa fa-caret-left"></i>
				<i class="fa fa-caret-right"></i>
				<i class="fa fa-columns"></i>
				<i class="fa fa-unsorte"></i>
				<i class="fa fa-sort"></i>
				<i class="fa fa-sort-dow"></i>
				<i class="fa fa-sort-desc"></i>
				<i class="fa fa-sort-u"></i>
				<i class="fa fa-sort-asc"></i>
				<i class="fa fa-envelope"></i>
				<i class="fa fa-linkedin"></i>
				<i class="fa fa-rotate-lef"></i>
				<i class="fa fa-undo"></i>
				<i class="fa fa-lega"></i>
				<i class="fa fa-gavel"></i>
				<i class="fa fa-dashboar"></i>
				<i class="fa fa-tachometer"></i>
				<i class="fa fa-comment-o"></i>
				<i class="fa fa-comments-o"></i>
				<i class="fa fa-flas"></i>
				<i class="fa fa-bolt"></i>
				<i class="fa fa-sitemap"></i>
				<i class="fa fa-umbrella"></i>
				<i class="fa fa-past"></i>
				<i class="fa fa-clipboard"></i>
				<i class="fa fa-lightbulb-o"></i>
				<i class="fa fa-exchange"></i>
				<i class="fa fa-cloud-download"></i>
				<i class="fa fa-cloud-upload"></i>
				<i class="fa fa-user-md"></i>
				<i class="fa fa-stethoscope"></i>
				<i class="fa fa-suitcase"></i>
				<i class="fa fa-bell-o"></i>
				<i class="fa fa-coffee"></i>
				<i class="fa fa-cutlery"></i>
				<i class="fa fa-file-text-o"></i>
				<i class="fa fa-building-o"></i>
				<i class="fa fa-hospital-o"></i>
				<i class="fa fa-ambulance"></i>
				<i class="fa fa-medkit"></i>
				<i class="fa fa-fighter-jet"></i>
				<i class="fa fa-beer"></i>
				<i class="fa fa-h-square"></i>
				<i class="fa fa-plus-square"></i>
				<i class="fa fa-angle-double-left"></i>
				<i class="fa fa-angle-double-right"></i>
				<i class="fa fa-angle-double-up"></i>
				<i class="fa fa-angle-double-down"></i>
				<i class="fa fa-angle-left"></i>
				<i class="fa fa-angle-right"></i>
				<i class="fa fa-angle-up"></i>
				<i class="fa fa-angle-down"></i>
				<i class="fa fa-desktop"></i>
				<i class="fa fa-laptop"></i>
				<i class="fa fa-tablet"></i>
				<i class="fa fa-mobile-phon"></i>
				<i class="fa fa-mobile"></i>
				<i class="fa fa-circle-o"></i>
				<i class="fa fa-quote-left"></i>
				<i class="fa fa-quote-right"></i>
				<i class="fa fa-spinner"></i>
				<i class="fa fa-circle"></i>
				<i class="fa fa-mail-repl"></i>
				<i class="fa fa-reply"></i>
				<i class="fa fa-github-alt"></i>
				<i class="fa fa-folder-o"></i>
				<i class="fa fa-folder-open-o"></i>
				<i class="fa fa-smile-o"></i>
				<i class="fa fa-frown-o"></i>
				<i class="fa fa-meh-o"></i>
				<i class="fa fa-gamepad"></i>
				<i class="fa fa-keyboard-o"></i>
				<i class="fa fa-flag-o"></i>
				<i class="fa fa-flag-checkered"></i>
				<i class="fa fa-terminal"></i>
				<i class="fa fa-code"></i>
				<i class="fa fa-mail-reply-al"></i>
				<i class="fa fa-reply-all"></i>
				<i class="fa fa-star-half-empt"></i>
				<i class="fa fa-star-half-ful"></i>
				<i class="fa fa-star-half-o"></i>
				<i class="fa fa-location-arrow"></i>
				<i class="fa fa-crop"></i>
				<i class="fa fa-code-fork"></i>
				<i class="fa fa-unlin"></i>
				<i class="fa fa-chain-broken"></i>
				<i class="fa fa-question"></i>
				<i class="fa fa-info"></i>
				<i class="fa fa-exclamation"></i>
				<i class="fa fa-superscript"></i>
				<i class="fa fa-subscript"></i>
				<i class="fa fa-eraser"></i>
				<i class="fa fa-puzzle-piece"></i>
				<i class="fa fa-microphone"></i>
				<i class="fa fa-microphone-slash"></i>
				<i class="fa fa-shield"></i>
				<i class="fa fa-calendar-o"></i>
				<i class="fa fa-fire-extinguisher"></i>
				<i class="fa fa-rocket"></i>
				<i class="fa fa-maxcdn"></i>
				<i class="fa fa-chevron-circle-left"></i>
				<i class="fa fa-chevron-circle-right"></i>
				<i class="fa fa-chevron-circle-up"></i>
				<i class="fa fa-chevron-circle-down"></i>
				<i class="fa fa-html5"></i>
				<i class="fa fa-css3"></i>
				<i class="fa fa-anchor"></i>
				<i class="fa fa-unlock-alt"></i>
				<i class="fa fa-bullseye"></i>
				<i class="fa fa-ellipsis-h"></i>
				<i class="fa fa-ellipsis-v"></i>
				<i class="fa fa-rss-square"></i>
				<i class="fa fa-play-circle"></i>
				<i class="fa fa-ticket"></i>
				<i class="fa fa-minus-square"></i>
				<i class="fa fa-minus-square-o"></i>
				<i class="fa fa-level-up"></i>
				<i class="fa fa-level-down"></i>
				<i class="fa fa-check-square"></i>
				<i class="fa fa-pencil-square"></i>
				<i class="fa fa-external-link-square"></i>
				<i class="fa fa-share-square"></i>
				<i class="fa fa-compass"></i>
				<i class="fa fa-toggle-dow"></i>
				<i class="fa fa-caret-square-o-down"></i>
				<i class="fa fa-toggle-u"></i>
				<i class="fa fa-caret-square-o-up"></i>
				<i class="fa fa-toggle-righ"></i>
				<i class="fa fa-caret-square-o-right"></i>
				<i class="fa fa-eur"></i>
				<i class="fa fa-eur"></i>
				<i class="fa fa-gbp"></i>
				<i class="fa fa-dolla"></i>
				<i class="fa fa-usd"></i>
				<i class="fa fa-rupe"></i>
				<i class="fa fa-inr"></i>
				<i class="fa fa-cn"></i>
				<i class="fa fa-rm"></i>
				<i class="fa fa-ye"></i>
				<i class="fa fa-jpy"></i>
				<i class="fa fa-rubl"></i>
				<i class="fa fa-roubl"></i>
				<i class="fa fa-rub"></i>
				<i class="fa fa-wo"></i>
				<i class="fa fa-krw"></i>
				<i class="fa fa-bitcoi"></i>
				<i class="fa fa-btc"></i>
				<i class="fa fa-file"></i>
				<i class="fa fa-file-text"></i>
				<i class="fa fa-sort-alpha-asc"></i>
				<i class="fa fa-sort-alpha-desc"></i>
				<i class="fa fa-sort-amount-asc"></i>
				<i class="fa fa-sort-amount-desc"></i>
				<i class="fa fa-sort-numeric-asc"></i>
				<i class="fa fa-sort-numeric-desc"></i>
				<i class="fa fa-thumbs-up"></i>
				<i class="fa fa-thumbs-down"></i>
				<i class="fa fa-youtube-square"></i>
				<i class="fa fa-youtube"></i>
				<i class="fa fa-xing"></i>
				<i class="fa fa-xing-square"></i>
				<i class="fa fa-youtube-play"></i>
				<i class="fa fa-dropbox"></i>
				<i class="fa fa-stack-overflow"></i>
				<i class="fa fa-instagram"></i>
				<i class="fa fa-flickr"></i>
				<i class="fa fa-adn"></i>
				<i class="fa fa-bitbucket"></i>
				<i class="fa fa-bitbucket-square"></i>
				<i class="fa fa-tumblr"></i>
				<i class="fa fa-tumblr-square"></i>
				<i class="fa fa-long-arrow-down"></i>
				<i class="fa fa-long-arrow-up"></i>
				<i class="fa fa-long-arrow-left"></i>
				<i class="fa fa-long-arrow-right"></i>
				<i class="fa fa-apple"></i>
				<i class="fa fa-windows"></i>
				<i class="fa fa-android"></i>
				<i class="fa fa-linux"></i>
				<i class="fa fa-dribbble"></i>
				<i class="fa fa-skype"></i>
				<i class="fa fa-foursquare"></i>
				<i class="fa fa-trello"></i>
				<i class="fa fa-female"></i>
				<i class="fa fa-male"></i>
				<i class="fa fa-gitti"></i>
				<i class="fa fa-gratipay"></i>
				<i class="fa fa-sun-o"></i>
				<i class="fa fa-moon-o"></i>
				<i class="fa fa-archive"></i>
				<i class="fa fa-bug"></i>
				<i class="fa fa-vk"></i>
				<i class="fa fa-weibo"></i>
				<i class="fa fa-renren"></i>
				<i class="fa fa-pagelines"></i>
				<i class="fa fa-stack-exchange"></i>
				<i class="fa fa-arrow-circle-o-right"></i>
				<i class="fa fa-arrow-circle-o-left"></i>
				<i class="fa fa-toggle-lef"></i>
				<i class="fa fa-caret-square-o-left"></i>
				<i class="fa fa-dot-circle-o"></i>
				<i class="fa fa-wheelchair"></i>
				<i class="fa fa-vimeo-square"></i>
				<i class="fa fa-turkish-lir"></i>
				<i class="fa fa-try"></i>
				<i class="fa fa-plus-square-o"></i>
				<i class="fa fa-space-shuttle"></i>
				<i class="fa fa-slack"></i>
				<i class="fa fa-envelope-square"></i>
				<i class="fa fa-wordpress"></i>
				<i class="fa fa-openid"></i>
				<i class="fa fa-institutio"></i>
				<i class="fa fa-ban"></i>
				<i class="fa fa-university"></i>
				<i class="fa fa-mortar-boar"></i>
				<i class="fa fa-graduation-cap"></i>
				<i class="fa fa-yahoo"></i>
				<i class="fa fa-google"></i>
				<i class="fa fa-reddit"></i>
				<i class="fa fa-reddit-square"></i>
				<i class="fa fa-stumbleupon-circle"></i>
				<i class="fa fa-stumbleupon"></i>
				<i class="fa fa-delicious"></i>
				<i class="fa fa-digg"></i>
				<i class="fa fa-pied-piper"></i>
				<i class="fa fa-pied-piper-alt"></i>
				<i class="fa fa-drupal"></i>
				<i class="fa fa-joomla"></i>
				<i class="fa fa-language"></i>
				<i class="fa fa-fax"></i>
				<i class="fa fa-building"></i>
				<i class="fa fa-child"></i>
				<i class="fa fa-paw"></i>
				<i class="fa fa-spoon"></i>
				<i class="fa fa-cube"></i>
				<i class="fa fa-cubes"></i>
				<i class="fa fa-behance"></i>
				<i class="fa fa-behance-square"></i>
				<i class="fa fa-steam"></i>
				<i class="fa fa-steam-square"></i>
				<i class="fa fa-recycle"></i>
				<i class="fa fa-automobil"></i>
				<i class="fa fa-car"></i>
				<i class="fa fa-ca"></i>
				<i class="fa fa-taxi"></i>
				<i class="fa fa-tree"></i>
				<i class="fa fa-spotify"></i>
				<i class="fa fa-deviantart"></i>
				<i class="fa fa-soundcloud"></i>
				<i class="fa fa-database"></i>
				<i class="fa fa-file-pdf-o"></i>
				<i class="fa fa-file-word-o"></i>
				<i class="fa fa-file-excel-o"></i>
				<i class="fa fa-file-powerpoint-o"></i>
				<i class="fa fa-file-photo-"></i>
				<i class="fa fa-file-picture-"></i>
				<i class="fa fa-file-image-o"></i>
				<i class="fa fa-file-zip-"></i>
				<i class="fa fa-file-archive-o"></i>
				<i class="fa fa-file-sound-"></i>
				<i class="fa fa-file-audio-o"></i>
				<i class="fa fa-file-movie-"></i>
				<i class="fa fa-file-video-o"></i>
				<i class="fa fa-file-code-o"></i>
				<i class="fa fa-vine"></i>
				<i class="fa fa-codepen"></i>
				<i class="fa fa-jsfiddle"></i>
				<i class="fa fa-life-bou"></i>
				<i class="fa fa-life-buo"></i>
				<i class="fa fa-life-save"></i>
				<i class="fa fa-suppor"></i>
				<i class="fa fa-life-ring"></i>
				<i class="fa fa-circle-o-notch"></i>
				<i class="fa fa-r"></i>
				<i class="fa fa-rebel"></i>
				<i class="fa fa-g"></i>
				<i class="fa fa-empire"></i>
				<i class="fa fa-git-square"></i>
				<i class="fa fa-git"></i>
				<i class="fa fa-y-combinator-squar"></i>
				<i class="fa fa-yc-squar"></i>
				<i class="fa fa-hacker-news"></i>
				<i class="fa fa-tencent-weibo"></i>
				<i class="fa fa-qq"></i>
				<i class="fa fa-wecha"></i>
				<i class="fa fa-weixin"></i>
				<i class="fa fa-sen"></i>
				<i class="fa fa-paper-plane"></i>
				<i class="fa fa-send-"></i>
				<i class="fa fa-paper-plane-o"></i>
				<i class="fa fa-history"></i>
				<i class="fa fa-circle-thin"></i>
				<i class="fa fa-header"></i>
				<i class="fa fa-paragraph"></i>
				<i class="fa fa-sliders"></i>
				<i class="fa fa-share-alt"></i>
				<i class="fa fa-share-alt-square"></i>
				<i class="fa fa-bomb"></i>
				<i class="fa fa-soccer-ball-"></i>
				<i class="fa fa-futbol-o"></i>
				<i class="fa fa-tty"></i>
				<i class="fa fa-binoculars"></i>
				<i class="fa fa-plug"></i>
				<i class="fa fa-slideshare"></i>
				<i class="fa fa-twitch"></i>
				<i class="fa fa-yelp"></i>
				<i class="fa fa-newspaper-o"></i>
				<i class="fa fa-wifi"></i>
				<i class="fa fa-calculator"></i>
				<i class="fa fa-paypal"></i>
				<i class="fa fa-google-wallet"></i>
				<i class="fa fa-cc-visa"></i>
				<i class="fa fa-cc-mastercard"></i>
				<i class="fa fa-cc-discover"></i>
				<i class="fa fa-cc-amex"></i>
				<i class="fa fa-cc-paypal"></i>
				<i class="fa fa-cc-stripe"></i>
				<i class="fa fa-bell-slash"></i>
				<i class="fa fa-bell-slash-o"></i>
				<i class="fa fa-trash"></i>
				<i class="fa fa-copyright"></i>
				<i class="fa fa-at"></i>
				<i class="fa fa-eyedropper"></i>
				<i class="fa fa-paint-brush"></i>
				<i class="fa fa-birthday-cake"></i>
				<i class="fa fa-area-chart"></i>
				<i class="fa fa-pie-chart"></i>
				<i class="fa fa-line-chart"></i>
				<i class="fa fa-lastfm"></i>
				<i class="fa fa-lastfm-square"></i>
				<i class="fa fa-toggle-off"></i>
				<i class="fa fa-toggle-on"></i>
				<i class="fa fa-bicycle"></i>
				<i class="fa fa-bus"></i>
				<i class="fa fa-ioxhost"></i>
				<i class="fa fa-angellist"></i>
				<i class="fa fa-cc"></i>
				<i class="fa fa-sheke"></i>
				<i class="fa fa-sheqe"></i>
				<i class="fa fa-ils"></i>
				<i class="fa fa-meanpath"></i>
				<i class="fa fa-buysellads"></i>
				<i class="fa fa-connectdevelop"></i>
				<i class="fa fa-dashcube"></i>
				<i class="fa fa-forumbee"></i>
				<i class="fa fa-leanpub"></i>
				<i class="fa fa-sellsy"></i>
				<i class="fa fa-shirtsinbulk"></i>
				<i class="fa fa-simplybuilt"></i>
				<i class="fa fa-skyatlas"></i>
				<i class="fa fa-cart-plus"></i>
				<i class="fa fa-cart-arrow-down"></i>
				<i class="fa fa-diamond"></i>
				<i class="fa fa-ship"></i>
				<i class="fa fa-user-secret"></i>
				<i class="fa fa-motorcycle"></i>
				<i class="fa fa-street-view"></i>
				<i class="fa fa-heartbeat"></i>
				<i class="fa fa-venus"></i>
				<i class="fa fa-mars"></i>
				<i class="fa fa-mercury"></i>
				<i class="fa fa-interse"></i>
				<i class="fa fa-transgender"></i>
				<i class="fa fa-transgender-alt"></i>
				<i class="fa fa-venus-double"></i>
				<i class="fa fa-mars-double"></i>
				<i class="fa fa-venus-mars"></i>
				<i class="fa fa-mars-stroke"></i>
				<i class="fa fa-mars-stroke-v"></i>
				<i class="fa fa-mars-stroke-h"></i>
				<i class="fa fa-neuter"></i>
				<i class="fa fa-genderless"></i>
				<i class="fa fa-facebook-official"></i>
				<i class="fa fa-pinterest-p"></i>
				<i class="fa fa-whatsapp"></i>
				<i class="fa fa-server"></i>
				<i class="fa fa-user-plus"></i>
				<i class="fa fa-user-times"></i>
				<i class="fa fa-hote"></i>
				<i class="fa fa-bed"></i>
				<i class="fa fa-viacoin"></i>
				<i class="fa fa-train"></i>
				<i class="fa fa-subway"></i>
				<i class="fa fa-medium"></i>
				<i class="fa fa-y"></i>
				<i class="fa fa-y-combinator"></i>
				<i class="fa fa-optin-monster"></i>
				<i class="fa fa-opencart"></i>
				<i class="fa fa-expeditedssl"></i>
				<i class="fa fa-battery-"></i>
				<i class="fa fa-battery-full"></i>
				<i class="fa fa-battery-"></i>
				<i class="fa fa-battery-three-quarters"></i>
				<i class="fa fa-battery-"></i>
				<i class="fa fa-battery-half"></i>
				<i class="fa fa-battery-"></i>
				<i class="fa fa-battery-quarter"></i>
				<i class="fa fa-battery-"></i>
				<i class="fa fa-battery-empty"></i>
				<i class="fa fa-mouse-pointer"></i>
				<i class="fa fa-i-cursor"></i>
				<i class="fa fa-object-group"></i>
				<i class="fa fa-object-ungroup"></i>
				<i class="fa fa-sticky-note"></i>
				<i class="fa fa-sticky-note-o"></i>
				<i class="fa fa-cc-jcb"></i>
				<i class="fa fa-cc-diners-club"></i>
				<i class="fa fa-clone"></i>
				<i class="fa fa-balance-scale"></i>
				<i class="fa fa-hourglass-o"></i>
				<i class="fa fa-hourglass-"></i>
				<i class="fa fa-hourglass-start"></i>
				<i class="fa fa-hourglass-"></i>
				<i class="fa fa-hourglass-half"></i>
				<i class="fa fa-hourglass-"></i>
				<i class="fa fa-hourglass-end"></i>
				<i class="fa fa-hourglass"></i>
				<i class="fa fa-hand-grab-"></i>
				<i class="fa fa-hand-rock-o"></i>
				<i class="fa fa-hand-stop-"></i>
				<i class="fa fa-hand-paper-o"></i>
				<i class="fa fa-hand-scissors-o"></i>
				<i class="fa fa-hand-lizard-o"></i>
				<i class="fa fa-hand-spock-o"></i>
				<i class="fa fa-hand-pointer-o"></i>
				<i class="fa fa-hand-peace-o"></i>
				<i class="fa fa-trademark"></i>
				<i class="fa fa-registered"></i>
				<i class="fa fa-creative-commons"></i>
				<i class="fa fa-gg"></i>
				<i class="fa fa-gg-circle"></i>
				<i class="fa fa-tripadvisor"></i>
				<i class="fa fa-odnoklassniki"></i>
				<i class="fa fa-odnoklassniki-square"></i>
				<i class="fa fa-get-pocket"></i>
				<i class="fa fa-wikipedia-w"></i>
				<i class="fa fa-safari"></i>
				<i class="fa fa-chrome"></i>
				<i class="fa fa-firefox"></i>
				<i class="fa fa-opera"></i>
				<i class="fa fa-internet-explorer"></i>
				<i class="fa fa-t"></i>
				<i class="fa fa-television"></i>
				<i class="fa fa-contao"></i>
				<i class="fa fa-500px"></i>
				<i class="fa fa-amazon"></i>
				<i class="fa fa-calendar-plus-o"></i>
				<i class="fa fa-calendar-minus-o"></i>
				<i class="fa fa-calendar-times-o"></i>
				<i class="fa fa-calendar-check-o"></i>
				<i class="fa fa-industry"></i>
				<i class="fa fa-map-pin"></i>
				<i class="fa fa-map-signs"></i>
				<i class="fa fa-map-o"></i>
				<i class="fa fa-map"></i>
				<i class="fa fa-commenting"></i>
				<i class="fa fa-commenting-o"></i>
				<i class="fa fa-houzz"></i>
				<i class="fa fa-vimeo"></i>
				<i class="fa fa-black-tie"></i>
				<i class="fa fa-fonticons"></i>
				<i class="fa fa-reddit-alien"></i>
				<i class="fa fa-edge"></i>
				<i class="fa fa-credit-card-alt"></i>
				<i class="fa fa-codiepie"></i>
				<i class="fa fa-modx"></i>
				<i class="fa fa-fort-awesome"></i>
				<i class="fa fa-usb"></i>
				<i class="fa fa-product-hunt"></i>
				<i class="fa fa-mixcloud"></i>
				<i class="fa fa-scribd"></i>
				<i class="fa fa-pause-circle"></i>
				<i class="fa fa-pause-circle-o"></i>
				<i class="fa fa-stop-circle"></i>
				<i class="fa fa-stop-circle-o"></i>
				<i class="fa fa-shopping-bag"></i>
				<i class="fa fa-shopping-basket"></i>
				<i class="fa fa-hashtag"></i>
				<i class="fa fa-bluetooth"></i>
				<i class="fa fa-bluetooth-b"></i>
				<i class="fa fa-percent"></i>
			</div>
		</section>
	</div>
</div>
<?php
exit;
}

?>