<?php

/**
 * Version 1.0.0
 */

add_action('admin_enqueue_scripts', 'admin_style');
function admin_style()
{
  // add bootstrap
  wp_enqueue_style('admin-styles-bootsrap_grid', plugins_url('als_gain_profit_contest/assets/css/bootstrap.min.css'));
  wp_enqueue_style('admin-styles-bootsrap_theme', plugins_url('als_gain_profit_contest/assets/css/bootstrap-theme.min.css'));

  // add js
  wp_enqueue_script('ajax',  plugins_url('als_gain_profit_contest/assets/js/ajax.js'), array('jquery'), '', true);
  wp_enqueue_script('main',  plugins_url('als_gain_profit_contest/assets/js/main.js'), array('jquery'), '', true);
  wp_enqueue_script('sweetalert',  'https://cdn.jsdelivr.net/npm/sweetalert2@8', array('ajax'), '', true);

  wp_localize_script('ajax', 'url_ajax', array(
    "url" => admin_url('admin-ajax.php')
  ));
}

// add script adn style in front end
add_action('wp_enqueue_scripts', 'add_script_front');
function add_script_front()
{
  // add bootstrap
  wp_enqueue_style('styles-bootsrap_grid', plugins_url('als_gain_profit_contest/assets/css/bootstrap.min.css'));
  wp_enqueue_style('styles-bootsrap_theme', plugins_url('als_gain_profit_contest/assets/css/bootstrap-theme.min.css'));

  // add js
  wp_enqueue_script('ajax',  plugins_url('als_gain_profit_contest/assets/js/ajax.js'), array('jquery'), '', true);
  wp_enqueue_script('sweetalert',  'https://cdn.jsdelivr.net/npm/sweetalert2@8', array('ajax'), '', true);

  wp_localize_script('ajax', 'url_ajax', array(
    "url" => admin_url('admin-ajax.php')
  ));
}
