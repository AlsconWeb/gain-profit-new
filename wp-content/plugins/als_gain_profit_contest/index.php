<?php
/*
Plugin Name: Gain Profit contest
Plugin URI: https://gitlab.com/AlsconWeb/gain-profit-plugin.git
Description: Plugin for the site "gain-profit.com" implements a contest for customers
Version: 1.0.1
Author: Имя Alex L
Author URI: https://freelance.ru/lovik
Author UpWork profile: https://www.upwork.com/o/profiles/users/_~015d44c720e5f4462a/
*/





/**
 * als_gain_profit_contest_init
 * Version 1.0.0
 */
add_action('plugins_loaded', 'als_gain_profit_contest_init');
function als_gain_profit_contest_init()
{
  global $wpdb;
  $table_name = $wpdb->get_blog_prefix() . "gain_profit_contest";

  $query_db = "CREATE TABLE " . $table_name . " ( `id` BIGINT(20) NOT NULL AUTO_INCREMENT , `wallet` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , `ip` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , PRIMARY KEY (`id`)) ENGINE = MyISAM ;";

  if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {

    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($query_db);
  } else {
    // The table is already in the database
    // get_contest($table_name);
  }
}

// add style and script
require_once __DIR__ . '/scripts.php';

// add page plugin
require_once __DIR__ . '/admin/add_page_plugin.php';

// add ajax delete table
require_once __DIR__ . '/admin/ajax_delete_table.php';

// add count max people
require_once __DIR__ . '/admin/add_count_max.php';

// add register shortcode
require_once __DIR__ . '/admin/register_shortcode_plugin.php';

// add wallet number
require_once __DIR__ . '/admin/ajax_add_wallet_number.php';

/**
 * get_contest
 * 
 * @param $table_name string
 * 
 * return Array()
 *   
 * Version 1.0.0
 */
function get_contest($table_name)
{
  global $wpdb;

  $rez = $wpdb->get_results("SELECT * FROM $table_name");

  return $rez;
}

/**
 * get_ip
 * 
 * @param $table_name string
 * @param $ip string
 * 
 * return Array() or false 
 * 
 * Version 1.0.0
 */
function get_ip($table_name, $ip)
{
  global $wpdb;

  $rez = $wpdb->get_results("SELECT * FROM `$table_name` WHERE `ip` LIKE '$ip'");

  if ($rez) {
    return $rez;
  } else {
    $rez = false;
    return $rez;
  }

  // return $rez;
}
