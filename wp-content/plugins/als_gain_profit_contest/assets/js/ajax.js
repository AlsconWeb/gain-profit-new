/**
 * Version 1.0.0
 */
jQuery(document).ready(function ($) {

  // delete table ajax
  $('.delete_btn').click(function (e) {
    e.preventDefault();

    var data = {
      action: "delete_table",
      delete: true
    }

    $.ajax({
      type: "post",
      url: url_ajax.url,
      data: data,
      success: function (res) {
        res = JSON.parse(res);
        if (res.status == 'ok') {
          Swal.fire({
            type: "success",
            title: 'Таблица удалена',
            onClose: function () {
              window.location.reload();
            },
          });
        }
      }
    });
  });

  // add count max user
  $('.count_max_users').on('submit', function (e) {
    e.preventDefault();
    var data = {
      action: "add_count_max",
      count: $('.wallet').val()
    }

    $.ajax({
      type: "post",
      url: url_ajax.url,
      data: data,
      success: function (res) {
        res = JSON.parse(res);
        if (res.status == 'ok') {
          Swal.fire({
            type: "success",
            title: 'Изменение сохранено',
            onClose: function () {
              window.location.reload();
            },
          });
        }
      }
    });
  });

  //add wallet number
  $('.user-wallet-number').submit(function (e) {
    e.preventDefault();
    var wallet_number = {
      action: "add_wallet_number",
      data: {
        number: $('.send-wallet').val(),
        ip: $('.ip-user').val(),
      }
    }

    $.ajax({
      type: "post",
      url: url_ajax.url,
      data: wallet_number,
      success: function (res) {
        res = JSON.parse(res);
        console.log(res);
        if (res.status == 'ok' && res.add_wall == 'on') {
          Swal.fire({
            type: "success",
            title: 'Ваш Кошелек был успешно добавлен',
            onClose: function () {
              window.location.reload();
            },
          });
        } else {
          Swal.fire({
            type: "error",
            title: 'Вы уже добовляли кошелек. Можно добавить только один кошелек',
            onClose: function () {
              window.location.reload();
            },
          });
        }
      }
    });
  });


  // add value in input
  var data_count = {
    action: "count_max",
  }

  $.ajax({
    type: "post",
    url: url_ajax.url,
    data: data_count,
    success: function (res) {
      // console.log(res);
      res = JSON.parse(res);
      if (res.status == 'ok') {
        $('.wallet').val(res.count);
      }
    }
  });

});