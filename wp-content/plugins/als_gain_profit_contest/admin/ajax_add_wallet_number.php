<?php

/**
 * Version 1.0.1
 */


add_action('wp_ajax_add_wallet_number', 'add_wallet_number');
add_action('wp_ajax_nopriv_add_wallet_number', 'add_wallet_number');

function add_wallet_number()
{
  // post var
  $number_wallet = $_POST['data']['number'];
  $ip = $_POST['data']['ip'];
  $cookie = $_COOKIE['contest'];




  global $wpdb;
  $table_name = $wpdb->get_blog_prefix() . "gain_profit_contest";

  // get ip in db
  $ip_in_db = get_ip($table_name, $ip);
  $count = get_option('als_count_max');
  $count_now = count(get_contest($table_name));

  if (!$cookie && $ip_in_db == false && $count_now < $count) {
    $number_wallet =  str_replace(' ', '', $number_wallet);
    $wpdb->insert($table_name, array('wallet' => $number_wallet, 'ip' => $ip));
    setcookie("contest", 'on', time() + 60 * 24 * 30, COOKIEPATH, COOKIE_DOMAIN);
    die(json_encode(array('status' => 'ok', 'add_wall' => 'on')));
  } else {
    die(json_encode(array('status' => 'ok', 'add_wall' => 'no')));
  }
}
