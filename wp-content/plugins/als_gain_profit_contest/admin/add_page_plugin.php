<?php

/**
 * Version 1.0.0
 */

add_action('admin_menu', 'register_plugin_page');
add_filter('option_page_capability_contest',  'contest', "capability");

// Add the menu item visibility for Editors.
function register_plugin_page()
{
  add_menu_page('Конкурс', 'Конкурс', 'edit_others_posts', 'contest', 'page_content_plugin', 'dashicons-tickets-alt', 6);
}
function capability($capability)
{
  return 'edit_others_posts';
}



function page_content_plugin()
{
  global $wpdb;
  $table_name = $wpdb->get_blog_prefix() . "gain_profit_contest";

  $content = get_contest($table_name);


  $table = "";
  if ($content) {
    foreach ($content as $val) {
      $table .= "<tr><td>" . $val->id . "</td><td>" . $val->wallet . "</td><td>" . $val->ip . "</td></tr>";
    }
  } else {
    $table .= "<tr><td></td><td></td><td></td></tr>";
  }

  $html = '<div class="container">
            <div class="row">
              <div class="col-xs-10">
                <h1>Настройки</h1>
                <form  method="post" class="count_max_users">
                  <div class="col-xs-4">
                    <input type="text" class="form-control wallet">
                    <span class="help-block">Количество мест</span>
                  </div>
                  <div class="col-xs-3">
                    <input type="submit" class="form-control btn btn-primary" value="Сохранить">
                  </div>
                </form>
                <div class="col-xs-2 pull-right"><button type="button" class="btn btn-danger delete_btn">Очистить таблицу</button></div>
                
              </div>
              <div class="row"><div class="col-xs-10"><hr>Добавь этот шорт код там где будут выводится форма с таблицей <kbd>[contest header-text="Ваш заголовок" title-text="Ваш текст" text-end-contest="Текст когда закнчилось местов конкурсе"]</kbd><hr></div></div>
              <div class="row">
                <div class="col-xs-10">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Кошелек</th>
                        <th>IP</th>
                      </tr>
                    </thead>
                    <tbody>
                      ' . $table . '
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>';



  echo $html;
}
