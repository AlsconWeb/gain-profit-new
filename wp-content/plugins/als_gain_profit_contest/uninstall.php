<?php

/**
 * Version 1.0.0
 */

if (!defined('WP_UNINSTALL_PLUGIN')) exit;
global $wpdb;

// Remove table from database
$table_name = $wpdb->get_blog_prefix() . "gain_profit_contest";
$sql = "DROP TABLE IF EXISTS $table_name";
$wpdb->query($sql);

delete_option('als_count_max');
