<?php
/**
 * Created 14.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP
 */

/**
 * Themes Init.
 */

use IWP\shortcodes\IWPButton;
use IWP\shortcodes\IWPHighlight;
use IWP\shortcodes\IWPIcon;

add_action( 'init', 'themes_init' );
/**
 * Themes Init
 */
function themes_init() {
	$highlight = new IWPHighlight();
	$button    = new IWPButton();
	$icon      = new IWPIcon();
}

add_action( 'wp_enqueue_scripts', 'add_scripts' );
/**
 * Add Scripts and Styles.
 */
function add_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_directory() . '/assets/css/style.css', '', '1.0.0', '' );
}

/**
 * Add autoload classes.
 */
require_once __DIR__ . '/vendor/autoload.php';
