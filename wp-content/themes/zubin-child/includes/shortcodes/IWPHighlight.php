<?php
/**
 * Created 14.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\shortcodes
 */

namespace IWP\shortcodes;

/**
 * Class IWPHighlight
 *
 * @package IWP\shortcodes
 */
class IWPHighlight {
	/**
	 * IWPHighlight constructor.
	 */
	public function __construct() {
		add_shortcode( 'highlight', array( $this, 'output' ) );

	}

	/**
	 * Output HTML.
	 *
	 * @param array  $atts    Attribute.
	 * @param string $content Content.
	 *
	 * @return string
	 */
	public function output( array $atts, string $content ): string {
		$atts = shortcode_atts( array( 'color' => 'black' ), $atts );

		return '<i class="highlight" style="background:' . $atts['color'] . '; color:#fff;">' . $content . '</i>';

	}

}
