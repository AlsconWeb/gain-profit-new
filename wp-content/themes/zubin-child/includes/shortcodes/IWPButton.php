<?php
/**
 * Created 14.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\shortcodes
 */

namespace IWP\shortcodes;

/**
 * Class IWPButton
 *
 * @package IWP\shortcodes
 */
class IWPButton {
	/**
	 * IWPButton constructor.
	 */
	public function __construct() {
		add_shortcode( 'button', [ $this, 'output' ] );
	}
	
	/**
	 * Output HTML.
	 *
	 * @param array  $atts    Attributes.
	 * @param string $content Content.
	 */
	public function output( array $atts, string $content ) {
		$atts = shortcode_atts(
			[
				'url'    => '#',
				'target' => '',
				'size'   => 'small',
				'color'  => '',
			],
			$atts
		);
		if ( ! empty( $atts['color'] ) ) {
			$style = 'background:' . $atts['color'] . ';';
		}
		
		return '<a class="button" href="' . $atts['url'] . '" target="' . $atts['target'] . '"
style="' . $style . '">' . do_shortcode( $content ) . '</a>';
	}
}
