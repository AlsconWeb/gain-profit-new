<?php
/**
 * Created 15.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\shortcodes;

/**
 * Class IWPIcon
 *
 * @package IWP\shortcodes
 */
class IWPIcon {
	/**
	 * IWPIcon constructor.
	 */
	public function __construct() {
		add_shortcode( 'icon', [ $this, 'output' ] );
	}
	
	/**
	 * Output HTML.
	 *
	 * @param array $atts Attributes.
	 */
	public function output( array $atts ): string {
		$atts = shortcode_atts(
			[
				'name' => '',
			],
			$atts
		);
		
		return '<i class="' . $atts['name'] . ' s-20"></i>';
	}
}
